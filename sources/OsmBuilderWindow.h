#ifndef _OSMBUILDERWINDOW_H_
#define _OSMBUILDERWINDOW_H_
#include "BaseFrame.h"
#include <Agmd3D/Core/SceneNode/SceneNode.h>
#include "geometryprocessing/FootPrint.h"

using namespace Agmd;

class App;

class OsmBuilderWindow : public BaseFrame {
private:
	std::map<Footprint*, wxTreeItemId> m_nodes;
	std::map<wxTreeItemId, Footprint*> m_nodesId; //reverse search
	std::map<wxTreeItemId, std::pair<int, Footprint*>> m_nodesFloorId; //reverse search

	std::vector<Footprint*>* m_footprints;
	Footprint* m_lastFootprintSelected;
	int m_lastFloorSelected;
	std::vector<Mesh*>* m_library;
	Agmd::SceneNode* m_buildingsParent;
	void addEnumProperty(std::vector<int>* styles_, std::string nameSelected_, std::string label_, Footprint* footprint_, int numFloor_);
	App* m_app;
	bool m_deleteOrAddIsRunning;

public:
	OsmBuilderWindow() :BaseFrame(NULL){}
	OsmBuilderWindow(App* app, std::vector<Mesh*>* library_, Agmd::SceneNode* buildingsParent_) : BaseFrame(NULL), m_library(library_)
		, m_buildingsParent(buildingsParent_), m_footprints(NULL), m_lastFootprintSelected(NULL), m_deleteOrAddIsRunning(false), m_app(app){}

	void updateProperties(Footprint* footprint_, int numFloor_); //-1 is footprint
	void updateSceneTree(std::vector<Footprint*>* footprints_ = NULL);
	void regenarationLastFootprint();

	//From BaseFrame
	virtual void onExport(wxCommandEvent& event);
	virtual void onImportOsm(wxCommandEvent& event);
	virtual void onChangedProperties(wxPropertyGridEvent& event);
	virtual void onSelect(wxTreeEvent& event);
	virtual void onClickAddFloor(wxCommandEvent& event);
	virtual void onClickRemoveFloor(wxCommandEvent& event);
	virtual void onClickRenderSolid(wxCommandEvent& event);
	virtual void onClickRenderWireframe(wxCommandEvent& event);
	virtual void onClickRenderPoints(wxCommandEvent& event);

};

#endif _OSMBUILDERWINDOW_H_