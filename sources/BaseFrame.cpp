///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "BaseFrame.h"

#include "../res/floor_minus.bmp.h"
#include "../res/floor_plus.bmp.h"
#include "../res/render_points.bmp.h"
#include "../res/render_solid.bmp.h"
#include "../res/render_wireframe.bmp.h"

///////////////////////////////////////////////////////////////////////////

MyFrame1::MyFrame1( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	m_menubar1 = new wxMenuBar( 0 );
	m_menu1 = new wxMenu();
	wxMenuItem* m_menuItem1;
	m_menuItem1 = new wxMenuItem( m_menu1, wxID_ANY, wxString( wxT("Import OSM") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu1->Append( m_menuItem1 );
	
	wxMenuItem* m_menuItem13;
	m_menuItem13 = new wxMenuItem( m_menu1, wxID_ANY, wxString( wxT("Exporter la sc�ne") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu1->Append( m_menuItem13 );
	
	m_menubar1->Append( m_menu1, wxT("Fichier") ); 
	
	m_menu2 = new wxMenu();
	wxMenuItem* m_menuItem21;
	m_menuItem21 = new wxMenuItem( m_menu2, wxID_ANY, wxString( wxT("Annuler") ) + wxT('\t') + wxT("Ctrl+Z"), wxEmptyString, wxITEM_NORMAL );
	m_menu2->Append( m_menuItem21 );
	
	wxMenuItem* m_menuItem211;
	m_menuItem211 = new wxMenuItem( m_menu2, wxID_ANY, wxString( wxT("R�p�ter") ) + wxT('\t') + wxT("Ctrl+Y"), wxEmptyString, wxITEM_NORMAL );
	m_menu2->Append( m_menuItem211 );
	
	wxMenuItem* m_menuItem2;
	m_menuItem2 = new wxMenuItem( m_menu2, wxID_ANY, wxString( wxT("Tout supprimer") ) + wxT('\t') + wxT("Ctrl+X"), wxEmptyString, wxITEM_NORMAL );
	m_menu2->Append( m_menuItem2 );
	
	m_menubar1->Append( m_menu2, wxT("Edition") ); 
	
	m_menu3 = new wxMenu();
	wxMenuItem* m_menuItem12;
	m_menuItem12 = new wxMenuItem( m_menu3, wxID_ANY, wxString( wxT("A propos") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu3->Append( m_menuItem12 );
	
	m_menubar1->Append( m_menu3, wxT("Aide") ); 
	
	this->SetMenuBar( m_menubar1 );
	
	wxFlexGridSizer* fgSizer1;
	fgSizer1 = new wxFlexGridSizer( 0, 3, 0, 0 );
	fgSizer1->SetFlexibleDirection( wxBOTH );
	fgSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	
	this->SetSizer( fgSizer1 );
	this->Layout();
	
	this->Centre( wxBOTH );
}

MyFrame1::~MyFrame1()
{
}

BaseFrame::BaseFrame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	m_mgr.SetManagedWindow(this);
	m_mgr.SetFlags(wxAUI_MGR_DEFAULT);
	
	m_menubar1 = new wxMenuBar( 0 );
	m_menu1 = new wxMenu();
	wxMenuItem* m_menuItem1;
	m_menuItem1 = new wxMenuItem( m_menu1, wxID_ANY, wxString( wxT("Import OSM") ) + wxT('\t') + wxT("Ctrl+O"), wxEmptyString, wxITEM_NORMAL );
	m_menu1->Append( m_menuItem1 );
	
	wxMenuItem* m_menuItem13;
	m_menuItem13 = new wxMenuItem( m_menu1, wxID_ANY, wxString( wxT("Exporter la sc�ne") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu1->Append( m_menuItem13 );
	
	m_menubar1->Append( m_menu1, wxT("Fichier") ); 
	
	m_menu2 = new wxMenu();
	wxMenuItem* m_menuItem21;
	m_menuItem21 = new wxMenuItem( m_menu2, wxID_ANY, wxString( wxT("Annuler") ) + wxT('\t') + wxT("Ctrl+Z"), wxEmptyString, wxITEM_NORMAL );
	m_menu2->Append( m_menuItem21 );
	
	wxMenuItem* m_menuItem211;
	m_menuItem211 = new wxMenuItem( m_menu2, wxID_ANY, wxString( wxT("R�p�ter") ) + wxT('\t') + wxT("Ctrl+Y"), wxEmptyString, wxITEM_NORMAL );
	m_menu2->Append( m_menuItem211 );
	
	wxMenuItem* m_menuItem2;
	m_menuItem2 = new wxMenuItem( m_menu2, wxID_ANY, wxString( wxT("Tout supprimer") ) + wxT('\t') + wxT("Ctrl+X"), wxEmptyString, wxITEM_NORMAL );
	m_menu2->Append( m_menuItem2 );
	
	m_menubar1->Append( m_menu2, wxT("Edition") ); 
	
	m_menu3 = new wxMenu();
	wxMenuItem* m_menuItem12;
	m_menuItem12 = new wxMenuItem( m_menu3, wxID_ANY, wxString( wxT("A propos") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu3->Append( m_menuItem12 );
	
	m_menubar1->Append( m_menu3, wxT("Aide") ); 
	
	this->SetMenuBar( m_menubar1 );
	
	m_treePanel = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxSize( 400,-1 ), wxTAB_TRAVERSAL );
	m_mgr.AddPane( m_treePanel, wxAuiPaneInfo() .Left() .Caption( wxT("Explorer") ).CloseButton( false ).PinButton( true ).Dock().Resizable().FloatingSize( wxSize( 110,90 ) ).Layer( 4 ) );
	
	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxVERTICAL );
	
	m_sceneTree = new wxTreeCtrl( m_treePanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE );
	bSizer2->Add( m_sceneTree, 1, wxALL|wxEXPAND, 5 );
	
	
	m_treePanel->SetSizer( bSizer2 );
	m_treePanel->Layout();
	m_viewPanel = new wxPanel( this, wxID_ANY, wxPoint( 0,0 ), wxSize( -1,-1 ), wxTAB_TRAVERSAL );
	m_mgr.AddPane( m_viewPanel, wxAuiPaneInfo() .Center() .CaptionVisible( false ).CloseButton( false ).PaneBorder( false ).Movable( false ).Dock().Resizable().FloatingSize( wxSize( -1,-1 ) ).DockFixed( true ).BottomDockable( false ).TopDockable( false ).LeftDockable( false ).RightDockable( false ).Floatable( false ).Row( 1 ).Layer( 4 ).CentrePane() );
	
	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer( wxVERTICAL );
	
	
	m_viewPanel->SetSizer( bSizer1 );
	m_viewPanel->Layout();
	bSizer1->Fit( m_viewPanel );
	m_propertiesPanel = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxSize( 400,-1 ), wxTAB_TRAVERSAL );
	m_mgr.AddPane( m_propertiesPanel, wxAuiPaneInfo() .Right() .Caption( wxT("Properties") ).CloseButton( false ).PinButton( true ).Dock().Resizable().FloatingSize( wxSize( 416,54 ) ).Layer( 1 ) );
	
	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer( wxVERTICAL );
	
	m_properties = new wxPropertyGrid(m_propertiesPanel, wxID_ANY, wxDefaultPosition, wxSize( 400,-1 ), wxPG_DEFAULT_STYLE);
	bSizer5->Add( m_properties, 1, wxALL|wxEXPAND, 5 );
	
	
	m_propertiesPanel->SetSizer( bSizer5 );
	m_propertiesPanel->Layout();
	m_toolBar3 = new wxToolBar( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTB_HORIZONTAL ); 
	m_addFloor = m_toolBar3->AddTool( wxID_ANY, wxT("tool"), floor_plus_bmp_to_wx_bitmap(), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_removeFloor = m_toolBar3->AddTool( wxID_ANY, wxT("tool"), floor_minus_bmp_to_wx_bitmap(), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_toolBar3->AddSeparator(); 
	
	m_renderSolid = m_toolBar3->AddTool( wxID_ANY, wxT("tool"), render_solid_bmp_to_wx_bitmap(), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_renderWireframe = m_toolBar3->AddTool( wxID_ANY, wxT("tool"), render_wireframe_bmp_to_wx_bitmap(), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_renderPoints = m_toolBar3->AddTool( wxID_ANY, wxT("tool"), render_points_bmp_to_wx_bitmap(), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_toolBar3->Realize();
	m_mgr.AddPane( m_toolBar3, wxAuiPaneInfo() .Top() .CaptionVisible( false ).CloseButton( false ).PaneBorder( false ).Movable( false ).Dock().Resizable().FloatingSize( wxSize( 322,94 ) ).Row( 0 ).Layer( 5 ) );
	
	
	
	m_mgr.Update();
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( m_menuItem1->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( BaseFrame::onImportOsm ) );
	this->Connect( m_menuItem13->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( BaseFrame::onExport ) );
	m_sceneTree->Connect( wxEVT_COMMAND_TREE_SEL_CHANGED, wxTreeEventHandler( BaseFrame::onSelect ), NULL, this );
	m_properties->Connect( wxEVT_PG_CHANGED, wxPropertyGridEventHandler( BaseFrame::onChangedProperties ), NULL, this );
	this->Connect( m_addFloor->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( BaseFrame::onClickAddFloor ) );
	this->Connect( m_removeFloor->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( BaseFrame::onClickRemoveFloor ) );
	this->Connect( m_renderSolid->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( BaseFrame::onClickRenderSolid ) );
	this->Connect( m_renderWireframe->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( BaseFrame::onClickRenderWireframe ) );
	this->Connect( m_renderPoints->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( BaseFrame::onClickRenderPoints ) );
}

BaseFrame::~BaseFrame()
{
	// Disconnect Events
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( BaseFrame::onImportOsm ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( BaseFrame::onExport ) );
	m_sceneTree->Disconnect( wxEVT_COMMAND_TREE_SEL_CHANGED, wxTreeEventHandler( BaseFrame::onSelect ), NULL, this );
	m_properties->Disconnect( wxEVT_PG_CHANGED, wxPropertyGridEventHandler( BaseFrame::onChangedProperties ), NULL, this );
	this->Disconnect( m_addFloor->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( BaseFrame::onClickAddFloor ) );
	this->Disconnect( m_removeFloor->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( BaseFrame::onClickRemoveFloor ) );
	this->Disconnect( m_renderSolid->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( BaseFrame::onClickRenderSolid ) );
	this->Disconnect( m_renderWireframe->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( BaseFrame::onClickRenderWireframe ) );
	this->Disconnect( m_renderPoints->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( BaseFrame::onClickRenderPoints ) );
	
	m_mgr.UnInit();
	
}
