#include "App.h"
#include <Agmd3D/Core/MediaManager.h>
#include <Agmd3D/Core/Driver.h>
#include <Agmd3D/Core/Enums.h>
#include <Agmd3D/Core/Declaration.h>
#include <Agmd3D/Core/DeclarationElement.h>
#include <Agmd3D/Core/ResourceManager.h>
#include <Agmd3D/Core/Model/GeometryFactory.h>

#include <Agmd3D/Core/Buffer/FrameBuffer.h>

#include <Agmd3D/Core/RenderObject/GraphicString.h>

#include <Agmd3D/Core/Model/Scene.h>
#include <Agmd3D/Core/Model/Water.h>
#include <Agmd3D/Core/Model/SkyBox.h>
#include <Agmd3D/Core/Model/Light.h>
#include <Agmd3D/Core/Model/Material.h>

#include <Agmd3D/Core/SceneNode/MeshNode.h>
#include <Agmd3D/Core/SceneNode/CameraNode.h>

#include <Agmd3D/Core/RenderingMode/DeferredRendering.h>
#include <Agmd3D/Core/RenderingMode/ForwardRendering.h>

#include <Agmd3D/Core/Camera/FPCamera.h>
#include <Agmd3D/Core/Camera/FollowCamera.h>

#include <AgmdUtilities/Utilities/Color.h>

#include <Agmd3D/Core/Effects/PostEffectMgr.h>
#include <Agmd3D/Core/Effects/AntiAliasing.h>

#include <Agmd3D/Core/Model/Model.h>



#include <Agmd3D/Core/GUI/GUIMgr.h>
#include <Agmd3D/Core/GUI/ASlider.h>
#include <Agmd3D/Core/GUI/AWindow.h>

#include <Agmd3D/Core/Shader/ShaderPreCompiler.h>
#include <Agmd3D/Core/Controller/FirstPersonController.h>


#include <Agmd3D/Core/Tools/Statistics.h>
#include <Renderer/OpenGL/GlDriver.h>
#include <Agmd3D/Core/Tools/Fast2DSurface.h>

#include <Agmd3D/Loaders/AssetLoader.h>

#include <fstream>

#include <glm/ext.hpp>
#include <libnoise/noise.h>

#define _USE_MATH_DEFINES
#include <math.h>
#include <random>

#include <Core/Shader/ShaderPipeline.h>
//#include "geometryprocessing\generation.cpp"
#include "geometryprocessing\PatternRecognition.h"
#include <map>



using namespace Agmd;
using namespace AgmdUtilities;

SINGLETON_IMPL(App);
#define WATER_RESOLUTION					2048
#define MODEL_SCALE							1.f
#define PATTERN_POSITION					3.0
#define PATTER_POSITION_POINT_OFFSET		0.1

SceneNode* createLight(LightType type, vec3 direction, float range = 1000.0f)
{
	Light* light = new Light(vec3(0,0,10), direction, type);
	light->SetRange(range);
	//light->SetAngles(40,80.0f);
	LightNode* l = new LightNode(NULL, light);

	return l;
}

void App::Run(int argc, char** argv)
{
	if(argc > 0)
	{
		main = File(argv[0]);
		MediaManager::Instance().AddSearchPath(main.Path());
		ShaderPreCompiler::Instance().AddSearchPath(main.Path()+"/Shader");
	}
	osmloader = new FootPrintLoader();
	m_FootprintsParent = new Agmd::SceneNode;
	m_RecognitionParent = new Agmd::SceneNode;
	m_BuildingsParent = new Agmd::SceneNode;

	wxEntryStart( argc, argv );
	AgmdApplication::Run(argc,argv);
}

void App::MakeWindow()
{
	
	wxApp::SetInstance(m_wxApplication);
	m_frame = m_osmBuilderWindow = new OsmBuilderWindow(this, &m_Library, m_BuildingsParent);
	this->CreateGlCanvas(m_osmBuilderWindow->m_viewPanel);
	
}

struct Group
{
	std::string name;
	std::vector<a_uint32> index;
};

void ParseTree(SceneNode* node,std::vector<Model::TVertex>& vertex,std::vector<a_uint32>& index,std::vector<Group>& group,a_uint32 index_offset,mat4& pmat)
{
	mat4 mat = node->getTransform().localModelMatrix()*pmat;
	if(node->getType() == MESH_NODE)
	{
		Group grp;
		MeshNode* mesh = static_cast<MeshNode*>(node);
		Model* model = static_cast<Model*>(mesh->getModel());
		auto vb = model->GetVertexBuffer();
		auto ib= model->GetIndexBuffer();
		Model::TVertex* _vertex =  vb.Lock();

		for(a_uint32 i = 0; i < vb.GetCount(); ++i){
			Model::TVertex v = _vertex[i];

			v.position = vec3(mat*vec4(v.position,1.0f));
			v.normal = mat3(mat)*v.position;
			vertex.push_back(v);
		}
		vb.Unlock();
		if(ib.GetCount())
		{
			Model::TIndex* _index =  ib.Lock();
			for(a_uint32 i = 0; i < ib.GetCount(); ++i){
				a_uint32 in = _index[i];
				in +=index_offset;
				index.push_back(in);
				grp.index.push_back(in);
			}
			ib.Unlock();
		}else
		{
			for(a_uint32 i = 0; i < vb.GetCount(); ++i){
				a_uint32 in = index_offset+i;
				index.push_back(in);
				grp.index.push_back(in);
			}
		}
		group.push_back(grp);
	}

	auto children = node->GetChilden();
	for(auto i = 0; i < children.size(); i++)
	{
		ParseTree(children[i],vertex,index,group,vertex.size(),mat);
	}
}

void App::Export(const char* path)
{
	std::ofstream stream(path,std::ios::out);
	std::vector<Model::TVertex> vertex;
	std::vector<a_uint32> index;
	std::vector<Group> group;
	ParseTree(m_BuildingsParent, vertex, index, group, 0, mat4(1));

	for(a_uint32 i = 0; i < vertex.size(); i++)
	{
		stream << "v " << vertex[i].position.x << " "<< vertex[i].position.y << " "<< vertex[i].position.z << endl;
	}
	for(a_uint32 i = 0; i < vertex.size(); i++)
	{
		stream << "vn " << vertex[i].normal.x << " "<< vertex[i].normal.y << " "<< vertex[i].normal.z << endl;
	}
	for(a_uint32 i = 0; i < vertex.size(); i++)
	{
		stream << "vt " << vertex[i].texCoords.x << " "<< vertex[i].texCoords.y << endl;
	}


	for(a_uint32 i = 0; i < index.size()/3; i++)
	{
		stream << "f " << (index[i*3]+1)<< "/"<<(index[i*3]+1)<<"/"<<(index[i*3]+1)<<" "<< (index[i*3+1]+1) << "/"<<(index[i*3+1]+1)<<"/"<<(index[i*3+1]+1)<< " "<< (index[i*3+2]+1) << "/"<<(index[i*3+2]+1)<<"/"<<(index[i*3+2]+1) << endl;
	}


	//by group failing :/
// 	for(a_uint32 i = 0; i < group.size(); i++)
// 	{
// 		Group& grp = group[i];
// 		stream << "g group" << i <<endl;
// 		for(a_uint32 j = 0; j < grp.index.size()/3; j++)
// 		{
// 			stream << "f " << (grp.index[j*3]+1)<< "/"<<(grp.index[j*3]+1)<<"/"<<(grp.index[j+3]+1)<<" "<< (grp.index[j+3+1]+1) << "/"<<(grp.index[j+3+1]+1)<<"/"<<(grp.index[j+3+1]+1)<< " "<< (grp.index[j+3+2]+1) << "/"<<(grp.index[j+3+2]+1)<<"/"<<(grp.index[j+3+2]+1) << endl;
// 
// 		}
// 	}
	stream.close();

}


void App::LoadOsm(const char* filename) {
	if (m_Footprints != NULL) {
		this->ClearFootprints();
	}

	m_Footprints = osmloader->LoadFromFile(filename);

	Model* mesh;
	MeshNode* node;
	// display footprints and recognition
	for (a_uint32 i = 0; i < m_Footprints->size(); ++i)
	{
		Footprint * footprint = (*m_Footprints)[i];

		Floor* floor = new Floor;
		floor->height = 4.f;
		floor->wallScale = 2.0;
		floor->pillarMesh = m_Library[PILIER];
		floor->windowMesh = m_Library[FENETRE];
		floor->wallMesh = m_Library[MUR];
		floor->arcMesh = m_Library[MUR];
		floor->pillarHeight = 2;

		footprint->_floors.push_back(floor);

		floor = new Floor;
		floor->height = 3.f;
		floor->wallScale = 2.0;
		floor->pillarMesh = m_Library[PILIER];
		floor->windowMesh = m_Library[FENETRE_SIMPLE];
		floor->wallMesh = m_Library[MUR_SIMPLE];
		floor->arcMesh = m_Library[MUR_SIMPLE];
		floor->pillarHeight = 2;

		footprint->_floors.push_back(floor);

		floor = new Floor;
		floor->height = 3.f;
		floor->wallScale = 2.0;
		floor->pillarMesh = m_Library[PILIER_GREC];
		floor->windowMesh = m_Library[FENETRE_SIMPLE];
		floor->wallMesh = m_Library[MUR_SIMPLE];
		floor->arcMesh = m_Library[MUR_SIMPLE];
		floor->pillarHeight = 2;

		footprint->_floors.push_back(floor);

		floor = new Floor;
		floor->height = 3.f;
		floor->wallScale = 2.0;
		floor->pillarMesh = m_Library[PILIER_GREC];
		floor->windowMesh = m_Library[FENETRE_SIMPLE];
		floor->wallMesh = m_Library[MUR_SIMPLE];
		floor->arcMesh = m_Library[MUR_SIMPLE];
		floor->pillarHeight = 2;

		footprint->_floors.push_back(floor);

		footprint->generateFootprintNode();
		this->m_FootprintsParent->addChild(footprint->_footprintNode);

		footprint->naiveRecognition();
		this->m_RecognitionParent->addChild(footprint->_recognitionNode);
	}

	this->Generate();
	m_osmBuilderWindow->updateSceneTree(m_Footprints);
}

void App::ClearFootprints() {
	for (int i = m_Footprints->size() - 1; i >= 0; --i) {
		ClearFootprint(i);
	}
}

void App::ClearFootprint(int index) {
	Footprint* fp = (*m_Footprints)[index];
	m_FootprintsParent->removeChild(fp->_footprintNode);
	m_RecognitionParent->removeChild(fp->_recognitionNode);
	m_BuildingsParent->removeChild(fp->_buildingNode);

	m_Footprints->erase(m_Footprints->begin() + index);

	delete fp;
}

void App::Generate() {
	for (a_uint32 i = 0; i < m_Footprints->size(); ++i)
	{
		std::cout << "footprint #" << i << "\n";
		Footprint * footprint = (*m_Footprints)[i];

		footprint->generateBuilding();
		this->m_BuildingsParent->addChild(footprint->_buildingNode);

	}
}

void App::SetRender(Render render) {
	ClearScene();

	if (render == Render::FOOTPRINTS) {
		m_Scene->AddNode(m_FootprintsParent);
	}
	else if (render == Render::RECOGNITION) {
		m_Scene->AddNode(m_RecognitionParent);
	}
	else if (render == Render::BUILDINGS) {
		m_Scene->AddNode(m_BuildingsParent);
	}
}

void App::ClearScene() {
	m_Scene->RemoveNode(m_FootprintsParent);
	m_Scene->RemoveNode(m_RecognitionParent);
	m_Scene->RemoveNode(m_BuildingsParent);
}

void App::init()
{
	m_MatProj3D = glm::perspective(35.0f, (float)getScreen().x / (float)getScreen().y, 0.01f, 1000.f);
	m_MatProj2D = ortho(0.0f,(float)getScreen().x,0.0f,(float)getScreen().y);
	ForwardRendering* mode = new ForwardRendering(getScreen());
	RenderingMode::setRenderingMode(mode);
	m_fps = new GraphicString(ivec2(0,getScreen().y-15),"",Color::black,"Arial",20);
	m_Scene = new SceneMgr();

	SceneNode* l1 = createLight(Agmd::LightType::LIGHT_POINT, vec3(1, 0, -1));
	m_Scene->AddNode(l1);

	GUIMgr& guimgr = GUIMgr::Instance();

	SkyBox* sk = new SkyBox();
	m_Scene->SetSkybox(sk);
 	Driver::Get().SetActiveScene(m_Scene);
	Driver::Get().SetCullFace(2);
	//cam3D = new Camera(m_MatProj3D);
	cam3D = new Camera(PROJECTION_PERSPECTIVE,ProjectionOption(vec2(getScreen()),35.0f));
	InputController* controller = new FirstPersonController();
	camNode = new CameraNode(cam3D,controller);
	camNode->setController(controller);
	m_Scene->AddNode(camNode);
	//cam2D = new Camera(m_MatProj2D);
	cam2D = new Camera(PROJECTION_ORTHO,ProjectionOption(vec4(0.0f,(float)getScreen().x,0.0f,(float)getScreen().y)));
	
	Camera::setCurrent(cam3D, CAMERA_3D);
	Camera::setCurrent(cam2D, CAMERA_2D);
	Driver::Get().Enable(TRenderParameter::RENDER_POINTSIZE_SHADER,true);

	m_Scene->AddNode(this->m_FootprintsParent);
	m_Scene->AddNode(this->m_RecognitionParent);
	m_Scene->AddNode(this->m_BuildingsParent);

	// load library
	Agmd::AssetLoader assetLoader;
	m_Library.push_back(assetLoader.LoadSingleMesh("../../res/window.obj", "Fenetre"));
	m_Library.push_back(assetLoader.LoadSingleMesh("../../res/simpleWindow.obj", "Fenetre simple"));
	m_Library.push_back(assetLoader.LoadSingleMesh("../../res/wall.obj", "Mur"));
	m_Library.push_back(assetLoader.LoadSingleMesh("../../res/simpleWall.obj", "Mur simple"));
	m_Library.push_back(assetLoader.LoadSingleMesh("../../res/pillar.obj", "Pilier"));
	m_Library.push_back(assetLoader.LoadSingleMesh("../../res/greekpillar.obj", "Pilier grec"));
	m_Library.push_back(assetLoader.LoadSingleMesh("../../res/triplePillar.obj", "Triple pilier"));
	m_Library.push_back(assetLoader.LoadSingleMesh("../../res/MRMWindow.obj", "Mur \"MRM\""));
	
	// generation
	m_Footprints = NULL;
	LoadOsm("../../res/single.osm");
}

void App::OnUpdate(a_uint64 time_diff/*in ms*/)
{
}

void App::OnRender3D()
{
}

void App::OnRender2D()
{

}

void App::OnClick( int click, vec2 pos,bool up)
{
	AgmdApplication::OnClick(click,pos,up);
}

void App::OnMove(vec2 pos)
{
	AgmdApplication::OnMove(pos);
}

void App::OnKey(a_char key, bool up)
{
	
	if (up) {
		if (key == 'I') {
			SetRender(Render::FOOTPRINTS);
		}
		else if (key == 'O') {
			SetRender(Render::RECOGNITION);
		}
		else if (key == 'P') {
			SetRender(Render::BUILDINGS);
		}
	}
	
	AgmdApplication::OnKey(key,up);
}