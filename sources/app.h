/*
============================================================================
Demo - A test application !

Author : 
Cyril Basset (basset.cyril@gmail.com - https://github.com/Agamand)
Jean-Vincent Lamberti (https://github.com/Kinroux)

https://github.com/Agamand/AgmdEngine
status : in pause
============================================================================
*/

#ifndef APP_H
#define APP_H
#include <Agmd3D/Config/Fwd.h>
#include <Agmd3D/Core/AgmdApplication.h>
#include <AgmdMaths/Vector2.h>
#include <AgmdMaths/Matrix4.h>
#include <AgmdUtilities/Utilities/Singleton.h>
#include <Agmd3D/Core/Model/Model.h>
#include <Agmd3D/Core/Model/SceneMgr.h>
#include <Agmd3D/Core/GUI/ASlider.h>
#include <map>
#include <ctime>
#include <Container/Vector.h>
#include <AgmdUtilities/Utilities/File.h>
#include "loader/FootPrintLoader.h"
#include "OsmBuilderWindow.h"

class FootPrintLoader;
#define SCREEN_WIDTH_PLANET 1600
#define SCREEN_HEIGHT_PLANET 900

enum Render {FOOTPRINTS, RECOGNITION, BUILDINGS};

class App : public Agmd::AgmdApplication, public Singleton<App>
{
    MAKE_SINGLETON(App);
public:
    virtual void Run(int argc, char** argv);
	AgmdUtilities::File& getMainDirectory() { return main; }
	static enum {
		FENETRE,
		FENETRE_SIMPLE,
		MUR,
		MUR_SIMPLE,
		PILIER,
		PILIER_GREC,
		TRIPLE_PILIER,
		MUR_MRM,
	};
	static std::vector<int> getPillarStyle() {
		std::vector<int> rst;
		rst.push_back(PILIER);
		rst.push_back(PILIER_GREC);
		rst.push_back(TRIPLE_PILIER);
		return rst; 
	};
	static std::vector<int> getWallStyle() {
		std::vector<int> rst;
		rst.push_back(MUR);
		rst.push_back(MUR_SIMPLE);
		rst.push_back(MUR_MRM);
		rst.push_back(FENETRE);
		rst.push_back(FENETRE_SIMPLE);
		return rst;
	};
	static std::vector<int> getWindowStyle() {
		std::vector<int> rst;
		rst.push_back(FENETRE);
		rst.push_back(FENETRE_SIMPLE);
		rst.push_back(MUR);
		rst.push_back(MUR_SIMPLE);
		rst.push_back(MUR_MRM);
		return rst;
	};

	void Export(const char* path);
	void LoadOsm(const char* filename);
	void ClearScene();
	void Generate();
	void ClearFootprints();
	void ClearFootprint(int index);
	void SetRender(Render render);

	Agmd::SceneNode* m_FootprintsParent;
	Agmd::SceneNode* m_RecognitionParent;
	Agmd::SceneNode* m_BuildingsParent;
private :
	App() : AgmdApplication("Sample 1")
	{}
    virtual void init();

    virtual void OnClick(int click, vec2 pos, bool up);
	virtual void OnMove(vec2 pos);
	virtual void OnKey(a_char key, bool up);

    virtual void OnUpdate(a_uint64 time_diff);

    virtual void OnRender3D();
    virtual void OnRender2D();

	virtual void MakeWindow();

    glm::mat4             m_MatProj2D;
    glm::mat4             m_MatProj3D;

    Agmd::SceneMgr*        m_Scene;
    Agmd::GraphicString*   m_fps;

    Agmd::Camera*       cam3D;
    Agmd::Camera*       cam2D;
	Agmd::SceneNode* camNode;
	FootPrintLoader* osmloader;
	

	AgmdUtilities::File main;

	std::vector<Footprint*>* m_Footprints;
	

	std::vector<Mesh*> m_Library;
	OsmBuilderWindow* m_osmBuilderWindow;
};

#endif // APP_H