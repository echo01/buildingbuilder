///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __BASEFRAME_H__
#define __BASEFRAME_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/sizer.h>
#include <wx/frame.h>
#include <wx/treectrl.h>
#include <wx/panel.h>
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/advprops.h>
#include <wx/toolbar.h>
#include <wx/aui/aui.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class MyFrame1
///////////////////////////////////////////////////////////////////////////////
class MyFrame1 : public wxFrame 
{
	private:
	
	protected:
		wxMenuBar* m_menubar1;
		wxMenu* m_menu1;
		wxMenu* m_menu2;
		wxMenu* m_menu3;
	
	public:
		
		MyFrame1( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Projet Annuel - ESGI"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 1024,720 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		
		~MyFrame1();
	
};

///////////////////////////////////////////////////////////////////////////////
/// Class BaseFrame
///////////////////////////////////////////////////////////////////////////////
class BaseFrame : public wxFrame 
{
	private:
	
	protected:
		wxMenuBar* m_menubar1;
		wxMenu* m_menu1;
		wxMenu* m_menu2;
		wxMenu* m_menu3;
		wxTreeCtrl* m_sceneTree;
		wxPropertyGrid* m_properties;
		wxToolBar* m_toolBar3;
		wxToolBarToolBase* m_addFloor; 
		wxToolBarToolBase* m_removeFloor; 
		wxToolBarToolBase* m_renderSolid; 
		wxToolBarToolBase* m_renderWireframe; 
		wxToolBarToolBase* m_renderPoints; 
		
		// Virtual event handlers, overide them in your derived class
		virtual void onImportOsm( wxCommandEvent& event ) { event.Skip(); }
		virtual void onExport( wxCommandEvent& event ) { event.Skip(); }
		virtual void onSelect( wxTreeEvent& event ) { event.Skip(); }
		virtual void onChangedProperties( wxPropertyGridEvent& event ) { event.Skip(); }
		virtual void onClickAddFloor( wxCommandEvent& event ) { event.Skip(); }
		virtual void onClickRemoveFloor( wxCommandEvent& event ) { event.Skip(); }
		virtual void onClickRenderSolid( wxCommandEvent& event ) { event.Skip(); }
		virtual void onClickRenderWireframe( wxCommandEvent& event ) { event.Skip(); }
		virtual void onClickRenderPoints( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		wxPanel* m_treePanel;
		wxPanel* m_viewPanel;
		wxPanel* m_propertiesPanel;
		
		BaseFrame( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Projet Annuel - ESGI"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 900,600 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		wxAuiManager m_mgr;
		
		~BaseFrame();
	
};

#endif //__BASEFRAME_H__
