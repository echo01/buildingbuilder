#pragma once

#include <glm\glm.hpp>

enum ShapeType {
	WALL,
	BALCONY,
	PILLAR,
	ARC
};

class Shape
{
public:
	virtual ShapeType getType() = 0;
	virtual ~Shape() {}
};

class Wall : public Shape
{
public:
	ShapeType getType() { return WALL; }
};

class Balcony : public Shape {
public:
	ShapeType getType() { return BALCONY; }
	float depth;
};

class Pillar : public Shape {
public:
	ShapeType getType() { return PILLAR; }
	float depth;
};

class Arc : public Shape {
public:
	ShapeType getType() { return ARC; }
	glm::dvec2 center;
	float angle;
	float radius;
	int resolution;
};

