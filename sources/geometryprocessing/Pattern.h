#ifndef PATTERN_H
#define PATTERN_H

#include <vector>
#include <glm\glm.hpp>

struct Pattern
{
	std::vector<glm::dvec2> _points;
	std::vector<glm::dvec2> _pointsResampled;
	std::vector<std::vector<glm::dvec2>> _sequences;
};

struct Result  {
	Pattern*	_pattern;
	double		_prob;
	std::vector<glm::dvec2> _pts;

	Result (Pattern* pattern_, double prob_, std::vector<glm::dvec2> pts_):_pattern(pattern_), _prob(prob_), _pts(pts_) {}
	/*int compareTo(Result* r) {
		if (prob == r.prob)
			return 0;
		else if (prob < r.prob)
			return 1;
		else
			return -1;
	}*/
};
struct IncrementalResult {
	Pattern*	_pattern;
	double		_prob;
	int			_indexOfMostLikelySegment;

	IncrementalResult (Pattern* pattern_, double prob_, int indexOfMostLikelySegment_):_pattern(pattern_),_prob(prob_),_indexOfMostLikelySegment(indexOfMostLikelySegment_) {}
};

#endif //PATTERN_H