
#ifndef FOOTPRINT_H
#define FOOTPRINT_H

#include "Shape.h"
#include <list>
#include <vector>
#include <map>
#include <iostream>
#include <string>
#include <limits>
#include <glm\glm.hpp>
#include <glm\gtx\vector_angle.hpp>
#include <CommonDefines.h>
#include <Agmd3D/Core/SceneNode/SceneNode.h>
#include <Agmd3D/Loaders/AssetLoader.h>

#define _USE_MATH_DEFINES
#define D_R (M_PI / 180.0)
#define R_D (180.0 / M_PI)
#include <math.h>

class FootprintNode
{
public:
	FootprintNode(const glm::dvec2& position) : _position(position) {}

	glm::dvec2 _position;
	glm::dvec2 _lonlat;
	glm::dvec2 _normal;
};

class FootprintEdge
{
public:
	FootprintEdge(const glm::dvec2& v1, const glm::dvec2& v2, a_uint32 i1, a_uint32 i2);
	a_uint32 _index;
	double _magnitude;
	a_uint32 _i1;
	a_uint32 _i2;
	glm::dvec2 _v1, _v2;
	glm::dvec2 _center;
	glm::dvec2 _direction;
	glm::dvec2 _normal;
	glm::dvec2 _normalEquation;
	glm::dvec2 _cornerBegin;
	glm::dvec2 _cornerEnd;

	//glm::mat4 _tbn;

	Shape* _shape;
};

class RecognizedEdge {
public:
	RecognizedEdge() {}
	RecognizedEdge(glm::dvec2 v1, glm::dvec2 v2, Shape* shape) : _v1(v1), _v2(v2), _shape(shape), _length(glm::length(v2 - v1)) {}
	glm::dvec2 _v1, _v2;
	float _length;
	Shape* _shape;
};

class ParallelEdges
{
public:
	std::vector<a_uint32> _edges;
	glm::dvec2 _direction;
	glm::dvec2 _origin;
	glm::dvec2 _sumDirections;
};

struct Floor
{
	float height;
	Agmd::Mesh* wallMesh;
	Agmd::Mesh* windowMesh;
	Agmd::Mesh* pillarMesh;
	Agmd::Mesh* arcMesh;
	float wallScale;
	int pillarHeight;		// pillar height (in floors, max: number of floors)
};

class Footprint
{
public:
	Footprint();
	Footprint(double, double);
	~Footprint();

	/* Regenerate the building without the recognition */
	void regenerateBuilding();

	/* delete building and recognition, keep original footprint */
	void reset(Agmd::SceneMgr* scene);

	/* set the outer polygon */
	void setOuterPolygon(const std::vector<glm::dvec2>&);

	/* Do the geometric recognition */
	void naiveRecognition();

	/* Create the original footprint mesh in _footprintNode */
	void generateFootprintNode();

	/* Create the 3D output in _buildingNode */
	void generateBuilding();

	// Footprint recognition attributes
	double _arcMaxAngle;				// max angle between two consecutives edges within an arc
	double _arcMinAngle;				// min angle between two consecutives edges within an arc
	double _pillarMaxDepth;				// max depth for a pillar
	double _balconyMaxDepth;			// max depth for a balcony
	unsigned int _arcResolution;		// resolution des arcs reconnus
	float _roofInset;
	float _roofHeight;
	float _windowMin;
	float _windowMax;
	std::vector<Floor*> _floors;		// each floor description

	// Geometry (2D)
	std::vector<FootprintNode> _outerPolygon;				// original outer polygon
	std::vector<std::vector<FootprintNode>> _innerPolygons;	// original inner polygons (not handled yet)
	std::vector<FootprintEdge> _edges;						// original polygon edges
	std::map<a_uint32, RecognizedEdge> _recognizedEdges;	// recognized edges
	std::vector<FootprintEdge> _outputPoly;					// output edges

	// Scene Nodes (3D Geometry)
	Agmd::SceneNode* _buildingNode;					// building (output)
	Agmd::SceneNode* _footprintNode;				// original footprint
	Agmd::SceneNode* _recognitionNode;				// recognized footprint
	

private:
	std::vector<a_uint32> _edgesToRecognize;				// list of all remaining edges to recognize during the recognition process
	void updateEdgesToRecognize(std::vector<a_uint32>& recognizedEdges);	// update remaining edges
	void recognizeArcs();				// arc recognition process
	void recognizeExtrusions();			// extrusions recognition process
	void processOutputPoly();			// output polygon process
};

#endif /* FOOTPRINT_H */
