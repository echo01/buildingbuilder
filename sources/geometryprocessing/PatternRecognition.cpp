#include "PatternRecognition.h"
PatternRecognition* PatternRecognition::_instance = NULL;
PatternRecognition& PatternRecognition::getInstance() {
	if (_instance == NULL) {
		_instance = new PatternRecognition();
	}

	return *_instance;
}
PatternRecognition::~PatternRecognition() {
	while(!_list.empty()) {
		delete(_list.back());
		_list.pop_back();
	}
}
/*************************************************************************************************

										RECOGNITION

*/////////////////////////////////////////////////////////////////////////////////////////////////
std::vector<glm::dvec2> PatternRecognition::footprintToVector(Footprint* footprint_)
{
	const  std::vector<FootprintNode>& outer = footprint_->_outerPolygon;
	std::vector<FootprintNode>::const_iterator it = outer.begin();
	std::vector<FootprintNode>::const_iterator end = outer.end();
	std::vector<glm::dvec2> vec;
	for(; it != end; ++it) {
		vec.push_back(glm::dvec2(it->_position));
	}
	return vec;
}
std::vector<Result*> PatternRecognition::recognize(std::vector<glm::dvec2>& input_, double beta_, double lambda_, double kappa_, double e_sigma_)
{
	if (input_.size() < 2) {
		printf("Input must consist of at least two points !");
		return std::vector<Result*>();
	}
	clock_t c = clock();
	std::vector<IncrementalResult*> incResults = getIncrementalResults(input_, beta_, lambda_, kappa_, e_sigma_);
	std::vector<Result*> results = getResults(incResults);
	//Collections.sort(results);	
	return results;
}
#define MICRO "�"
#define PREFIX_START (-24)
char* PatternRecognition::displayBigValueDebug(double value, int digits, int numeric)
{
	static char *prefix[] = {"y", "z", "a", "f", "p", "n", MICRO, "m", "","k", "M", "G", "T", "P", "E", "Z", "Y"}; 
	#define PREFIX_END (PREFIX_START+(int)((sizeof(prefix)/sizeof(char *)-1)*3))

	int expof10;
	static char result[100];
	char *res = result;

	if (value < 0.)
	{
		*res++ = '-';
		value = -value;
	}
	if (value == 0.)
	{
	return "0.0";
	}

	expof10 = (int) log10(value);
	if(expof10 > 0)
	expof10 = (expof10/3)*3;
	else
	expof10 = (-expof10+3)/3*(-3); 
 
	value *= pow(10,-(float)expof10);

	if (value >= 1000.)
		{ value /= 1000.0; expof10 += 3; }
	else if(value >= 100.0)
		digits -= 2;
	else if(value >= 10.0)
		digits -= 1;

	if(numeric || (expof10 < PREFIX_START) ||    
				(expof10 > PREFIX_END))
	sprintf(res, "%.*fe%d", digits-1, value, expof10); 
	else
	sprintf(res, "%.*f %s", digits-1, value, 
		prefix[(expof10-PREFIX_START)/3]);
	return result;
}
/*************************************************************************************************

										CREATE PATTERN

*/////////////////////////////////////////////////////////////////////////////////////////////////
void PatternRecognition::createPointsFromPatterns()
{
	std::vector<Pattern*>::iterator it = _list.begin();
	std::vector<Pattern*>::iterator end = _list.end();
	int resamplingPointCount;
	Pattern* pattern;
	printf("=============== CREATE SUB-PATTERNS(SEQUENCES) FROM PATTERNS\n");
	clock_t begin = clock();
	for (; it != end; ++it) {
		clock_t beginPattern = clock();
		pattern = *it;

		normalize(pattern->_points);
		
		//For one pattern, resampling points to have an EQUIDISTANT points list
		resamplingPointCount = getResamplingPointCount(pattern->_points, SPACING_BETWEEN_POINTS_GLOBALPATTERN);
		PatternRecognition::resample(pattern->_points, pattern->_pointsResampled, resamplingPointCount);
		printf("\t[time] Resampling %d Points : %f secs\n", pattern->_pointsResampled.size(), (double(clock() - beginPattern) / CLOCKS_PER_SEC));

		//Create sequences (sub patterns)
		clock_t beginsequence = clock();
		size_t size = (*it)->_pointsResampled.size();
#ifdef DONT_USE_PATTERN_SEGMENTATION
		int i = size-1;
#else
		int i = 1;
#endif
		for (; i < size; ++i) 
		{
			
			std::vector<glm::dvec2> tmpSequence = std::vector<glm::dvec2>(pattern->_pointsResampled.begin(), pattern->_pointsResampled.begin()+(i+1)); //i+1 because we want the fake end() iterator (past-the-end element)
			normalize(tmpSequence); //Normalize or not ?

			pattern->_sequences.push_back(std::vector<glm::dvec2>());
			resamplingPointCount = getResamplingPointCount(tmpSequence, SPACING_BETWEEN_POINTS_SEQUENCES_IN_PATTERN);
			resample(tmpSequence, pattern->_sequences.back(), resamplingPointCount); //use .back() to avoid copy of tmpSequence when we use .push_back()
		}
		printf("\t\t[time] Creating %d Sequences : %f secs\n", pattern->_sequences.size(), (double(clock() - beginsequence) / CLOCKS_PER_SEC));
	}
	printf("[time] Analysing %d Patterns : %f secs\n", _list.size(), (double(clock() - begin) / CLOCKS_PER_SEC));
}
void PatternRecognition::getPatternsFromFile()
{
	std::ifstream file(PATTERNS_FILE);
	std::string line;
	std::string token;
	double x, y;
	while(!file.eof())
	{
		getline(file,line);
		std::stringstream ss(line);
		Pattern* pattern = new Pattern();
		int i = 0;
		while(std::getline(ss, token, ','))
		{
			switch(i) {
				case 0:
					x = stof(token);
					break;
				case 1:
					y = stof(token);
					pattern->_points.push_back(glm::dvec2(x, y));
					i = -1;
					break;
			}
			++i;
		}
		_list.push_back(pattern);
	}
}
std::vector<glm::dvec2> PatternRecognition::searchPatternsDebug(std::vector<glm::dvec2> points_, int min_, int max_)
{
	std::vector<glm::dvec2> res;
	//int min = 72;
	//int max = 79; 
	
	//Max th will be ignore (only v1 will be display)
	int size = points_.size();
	//assert(size >= max_, "Size > max_ in PatternRecognition::searchPatternsDebug");
	if (size < max_)
		return res;

	printf("\n===================================\n");
	double x = points_.at(min_).x;
	double y = points_.at(min_).y;
	for (int i = min_ ; i < max_; ++i) {
		res.push_back(glm::dvec2(points_.at(i).x, points_.at(i).y));
		printf("%f,%f\n", points_.at(i).x, points_.at(i).y);
	}
	printf("\n===================================\n");

	return res;
}
/*************************************************************************************************

							TOOLS			RECOGNITION

*/////////////////////////////////////////////////////////////////////////////////////////////////
std::vector<Result*> PatternRecognition::getResults(std::vector<IncrementalResult*>& incrementalResults_) 
{
	std::vector<Result*> results;
	std::vector<IncrementalResult*>::iterator it = incrementalResults_.begin();
	std::vector<IncrementalResult*>::iterator end = incrementalResults_.end();
	for (; it != end; ++it) {
		results.push_back(new Result((*it)->_pattern, (*it)->_prob, (*it)->_pattern->_sequences.at((*it)->_indexOfMostLikelySegment)));
	}
	return results;
}
void PatternRecognition::rotatePattern(Pattern* pattern_, double angle_) {
	std::vector<glm::dvec2> res;

	for (size_t j = 0, sizeJ = pattern_->_sequences.size(); j < sizeJ; ++j) {
		for (size_t i = 0, size = pattern_->_sequences.at(j).size(); i < size; ++i) {
			pattern_->_sequences.at(j)[i] = glm::rotate(pattern_->_sequences.at(j).at(i), angle_);
		}
	}

	//Delete that: 
	for (size_t i = 0, size = pattern_->_points.size(); i < size; ++i) {
		pattern_->_points[i] = glm::rotate(pattern_->_points.at(i), angle_);
	}
}
std::vector<IncrementalResult*> PatternRecognition::getIncrementalResults(std::vector<glm::dvec2>& input_, double beta_, double lambda_, double kappa_, double e_sigma_)
{	
	std::vector<IncrementalResult*> results;
	std::vector<glm::dvec2> unkPts = std::vector<glm::dvec2>(input_);

	normalize(unkPts);

	std::vector<Pattern*>::iterator it = _list.begin();
	std::vector<Pattern*>::iterator end = _list.end();
	for (; it != end; ++it) {
		Pattern* pattern = *it;
		//Rotate pattern
		//double turningAngleOfFirstPoints = abs(getTurningAngleDistance(input_.at(0), input_.at(1), pattern->_points.at(0), pattern->_points.at(1)));
		//turningAngleOfFirstPoints = (turningAngleOfFirstPoints * 180) / M_PI;
		//rotatePattern(pattern, turningAngleOfFirstPoints);

		//double test = abs(getTurningAngleDistance(input_.at(0), input_.at(1), pattern->_sequences.at(0).at(0), pattern->_sequences.at(0).at(1)));

		//Get some results
		IncrementalResult* result = getIncrementalResult(unkPts, pattern, beta_, lambda_, e_sigma_);

		std::vector<glm::dvec2> lastSequencePts = pattern->_sequences.at(pattern->_sequences.size()-1);
		std::vector<glm::dvec2> unkPtsResampled;

		resample(unkPts, unkPtsResampled, lastSequencePts.size());
		double completeProb = getLikelihoodOfMatch(unkPtsResampled, lastSequencePts, e_sigma_, e_sigma_/beta_, lambda_);
		double x = 1 - completeProb;
		result->_prob *= (1 + kappa_*exp(-x*x));
		results.push_back(result);
	}
	marginalizeIncrementalResults(results);
	return results;
}
IncrementalResult* PatternRecognition::getIncrementalResult(std::vector<glm::dvec2>& unkPts_, Pattern* pattern_, double beta_, double lambda_, double e_sigma_) 
{
	double maxProb = -1;
	int maxIndex = -1;
	for (size_t i = 0, size = pattern_->_sequences.size(); i < size; ++i) 
	{
		clock_t	c2 = clock();
		std::vector<glm::dvec2> unkResampledPts;
		int samplingPtCount = pattern_->_sequences.at(i).size();
		resample(unkPts_, unkResampledPts, samplingPtCount);
		double prob = getLikelihoodOfMatch(unkResampledPts, pattern_->_sequences.at(i), e_sigma_, e_sigma_/beta_, lambda_);
		if (prob > maxProb) {
			maxProb = prob;
			maxIndex = i;
		}
	}
	return new IncrementalResult(pattern_, maxProb, maxIndex);
}
double PatternRecognition::getLikelihoodOfMatch(std::vector<glm::dvec2>& pts1_, std::vector<glm::dvec2>& pts2_, double eSigma_, double aSigma_, double lambda_) 
{
	assert(eSigma_ > 0, "eSigma must be positive");
	assert(aSigma_ > 0, "aSigma must be positive");
	assert((0 < lambda_ && lambda_ < 1), "lambda must be in the range between zero and one");

	double x_e = getEuclidianDistance(pts1_, pts2_);
	double x_a = getTurningAngleDistance(pts1_, pts2_);
	return exp(- (x_e * x_e / (eSigma_ * eSigma_) * lambda_ + x_a * x_a / (aSigma_ * aSigma_) * (1 - lambda_)));
}
double PatternRecognition::getEuclidianDistance(std::vector<glm::dvec2>& pts1_, std::vector<glm::dvec2>& pts2_)
{
	assert((pts1_.size() == pts2_.size()), "lists must be of equal lengths, cf. %d with %d", pts1_.size(), pts2_.size());

	int n = pts1_.size();
	double td = 0;
	for (int i = 0; i < n; i++) {
		td += glm::length(pts2_.at(i) - pts1_.at(i));
	}
	return td / n;
}
double PatternRecognition::getTurningAngleDistance(std::vector<glm::dvec2>& pts1_, std::vector<glm::dvec2>& pts2_) 
{
	assert(pts1_.size() == pts2_.size(), "lists must be of equal lengths, cf. %d with %d", pts1_.size(), pts2_.size());

	int n = pts1_.size();
	double td = 0;
	for (int i = 0; i < n - 1; i++) {
		td += abs(getTurningAngleDistance(pts1_.at(i), pts1_.at(i + 1), pts2_.at(i), pts2_.at(i + 1)));
	}
	return td / (n - 1);
}
double PatternRecognition::getTurningAngleDistance(glm::dvec2& ptA1_, glm::dvec2& ptA2_, glm::dvec2& ptB1_, glm::dvec2& ptB2_) 
{
	glm::dvec2 v1 = ptA2_ - ptA1_;
	glm::dvec2 v2 = ptB2_ - ptB1_;
	double len_a = glm::length(v1);
	double len_b = glm::length(v2);
	if (len_a == 0 || len_b == 0) {
		return 0;
	} else {
		float cos = glm::dot(v1, v2) / len_a / len_b;
		if (abs(cos) > 1.0) {
			return 0;
		} else {
			return acos(cos);
		}
	}
}
void PatternRecognition::marginalizeIncrementalResults(std::vector<IncrementalResult*>& results) 
{
	double totalMass = 0;
	std::vector<IncrementalResult*>::iterator it = results.begin();
	std::vector<IncrementalResult*>::iterator end = results.end();
	for (; it != end; ++it) {
		totalMass += (*it)->_prob; 
	}
	it = results.begin();
	for (; it != end; ++it) {
		(*it)->_prob += totalMass; 
	}
}
/*************************************************************************************************

							TOOLS			PATTERNS

*/////////////////////////////////////////////////////////////////////////////////////////////////
void PatternRecognition::resample(std::vector<glm::dvec2>& list_, std::vector<glm::dvec2>& listOUT_, size_t countTargetPoints)
{
	double len = getSpatialLength(list_);
	double segmentLen = len / (countTargetPoints - 1);
	std::vector<int> segmentPoints;
	int segmentPoint;
	//printf("Segment Length : %f\n", segmentLen);
	double rest = getSegmentPoints(list_, segmentPoints, segmentLen);

	//double horizRest = 0;
	//double verticRest = 0;
	double dx, dy;
	double x1, y1, x2, y2;
	int i, j;
	int sizeList = list_.size();
	x1 = list_.at(0).x;
	y1 = list_.at(0).y;
	for (int i = 1; i < sizeList; ++i) {
		x2 = list_.at(i).x;
		y2 = list_.at(i).y;
		segmentPoint = segmentPoints.at(i-1);
		if (segmentPoint - 1 <= 0) {
			dx = 0;
			dy = 0;
		} else {
			dx = (x2 - x1) / (double)(segmentPoint);
			dy = (y2 - y1) / (double)(segmentPoint);
		}
		if (segmentPoint > 0) {
			for (j = 0; j < segmentPoint; j++) {
				if (listOUT_.size() >= countTargetPoints-1) //Do not define the last point
					break;

				if (j == 0) {
					listOUT_.push_back(glm::dvec2(x1/* + horizRest*/, y1/* + verticRest*/)); 
					//horizRest = 0;
					//verticRest = 0;
				} else {
					listOUT_.push_back(glm::dvec2(x1 + j * dx, y1 + j * dy));
				}
			}
		}
		x1 = x2;
		y1 = y2;
	}

	int sizeOut = listOUT_.size();

	for (i = sizeOut; i < countTargetPoints - 1; ++i) { //Do not define the last point
		listOUT_.push_back(glm::dvec2((listOUT_.at(i-1).x + list_.at(sizeList - 1).x) / 2
									, (listOUT_.at(i-1).y + list_.at(sizeList - 1).y) / 2));
	}

	//Define the last point
	listOUT_.push_back(glm::dvec2(list_.at(sizeList - 1).x, list_.at(sizeList - 1).y));
}
int PatternRecognition::getResamplingPointCount(std::vector<glm::dvec2>& list_, float spacing_between_points_) 
{
	double len = getSpatialLength(list_);
	return (int)(len / spacing_between_points_) + 1;
}
double PatternRecognition::getSpatialLength(std::vector<glm::dvec2>& list_) 
{
	double len = 0.0;
	std::vector<glm::dvec2>::iterator it = list_.begin();
	std::vector<glm::dvec2>::iterator end = list_.end();
	std::vector<glm::dvec2>::iterator prev = it++; //prev = --it  && it = ++prev
	for ( ; it != end; ++it) {
		len += glm::distance(*prev, *it);
		prev = it;
	}
	return len;
}
double PatternRecognition::getSegmentPoints(std::vector<glm::dvec2>& list_, std::vector<int>& segmentPoints_, double length_)
{
	//printf("getSegmentPoints : ");
	double rest, currentLen;
	int ps;
	rest = 0.0f;
	std::vector<glm::dvec2>::iterator it = list_.begin();
	std::vector<glm::dvec2>::iterator end = list_.end();
	std::vector<glm::dvec2>::iterator prev = it;
	bool isFirst = true;
	for (++it; it != end; ++it) 
	{
		currentLen = glm::distance(*prev, *it);
		currentLen += rest;
		rest = 0.0f;
		ps = (int) ((currentLen / length_));
		if (ps == 0) {
			rest += currentLen;
		}
		else {
			rest += currentLen - (ps * length_);
		}
		if (isFirst) {
			if (ps == 0) {
				ps = 1;
			}
			isFirst = false;
		}
		segmentPoints_.push_back(ps);
		//printf("%d, ", ps);
		prev = it;
	}
	//printf("\n");
	return rest;
}
void PatternRecognition::normalize(std::vector<glm::dvec2>& list_)
{
	//SCALE TO NORMALIZED SPACE
	scaleTo(list_, NORMALIZE_SPACE_SQUARE_SIZE);
	glm::dvec2 centroid = getCentroid(list_);
	translate(list_, -centroid.x, -centroid.y);
}
void PatternRecognition::scaleTo(std::vector<glm::dvec2>& list_, double sizeOfNormalizedSpace_)
{
	glm::dvec4 bounds = getBoundingBox(list_);
	double a1 = sizeOfNormalizedSpace_;
	double a2 = sizeOfNormalizedSpace_;
	double b1 = bounds[2] - bounds[0];
	double b2 = bounds[3] - bounds[1];
	double globalScale = sqrt(a1 * a1 + a2 * a2) / sqrt(b1 * b1 + b2 * b2);
	scale(list_, globalScale, globalScale, bounds[0], bounds[1]);
}
void PatternRecognition::scale(std::vector<glm::dvec2>& list_, double scaleX_, double scaleY_, double originX_, double originY_)
{
	translate(list_, -originX_, -originY_);
	scale(list_, scaleX_, scaleY_);
	translate(list_, originX_, originY_);
}
void PatternRecognition::scale(std::vector<glm::dvec2>& list_, double scaleX_, double scaleY_)
{
	std::vector<glm::dvec2>::iterator it = list_.begin();
	std::vector<glm::dvec2>::iterator end = list_.end();
	for (; it != end; ++it) {
		it->x *= scaleX_;
		it->y *= scaleY_;
	}
}
void PatternRecognition::translate(std::vector<glm::dvec2>& list_, double offsetX_, double offsetY_)
{
	std::vector<glm::dvec2>::iterator it = list_.begin();
	std::vector<glm::dvec2>::iterator end = list_.end();
	for (; it != end; ++it) {
		it->x += offsetX_;
		it->y += offsetY_;
	}
}
glm::dvec2 PatternRecognition::getCentroid(std::vector<glm::dvec2>& list_)
{
	double totalMass = list_.size();
	double xIntegral = 0.0;
	double yIntegral = 0.0;
	std::vector<glm::dvec2>::iterator it = list_.begin();
	std::vector<glm::dvec2>::iterator end = list_.end();
	for (; it != end; ++it) {
		xIntegral += it->x;
		yIntegral += it->y;
	}
	return glm::dvec2(xIntegral/totalMass, yIntegral/totalMass);
}
glm::dvec4 PatternRecognition::getBoundingBox(std::vector<glm::dvec2>& list_)
{
	std::vector<glm::dvec2>::iterator it = list_.begin();
	std::vector<glm::dvec2>::iterator end = list_.end();
	double minX = it->x;
	double maxX = it->x;
	double minY = it->y;
	double maxY = it->y;
	for (++it; it != end; ++it) {
		double x = it->x;
		double y = it->y;
		if (x < minX)
			minX = x;
		if (x > maxX)
			maxX = x;
		if (y < minY)
			minY = y;
		if (y > maxY)
			maxY = y;
	}
	return glm::dvec4(minX, minY, maxX, maxY);
}