#ifndef PATTERN_RECOGNITION_H
#define PATTERN_RECOGNITION_H

#include <ctime>
#include <glm\glm.hpp>
#include <vector>
#include <fstream>
#include <sstream>
#include <limits>
#include "FootPrint.h"
#include "Pattern.h"

#define SPACING_BETWEEN_POINTS_GLOBALPATTERN					0.4 //0.1
#define SPACING_BETWEEN_POINTS_SEQUENCES_IN_PATTERN				0.4 //0.01
//#define PATTERNS_FILE											"../../res/patterns.txt"
#define PATTERNS_FILE											"../../res/patternsfull.txt"
#define	NORMALIZE_SPACE_SQUARE_SIZE								10.0

#define	DEFAULT_E_SIGMA											200.0
#define	DEFAULT_BETA											400.0
#define	DEFAULT_LAMBDA											0.4
#define	DEFAULT_KAPPA											1.0

#define THRESHOLD_PROBABILITY_PATTERN_RECOGNITION				3.8
#define DONT_USE_PATTERN_SEGMENTATION

class PatternRecognition
{
public:
	std::vector<Pattern*> _list;

	// -------------------------------- METHODS --------------------------------
	virtual ~PatternRecognition();
	static PatternRecognition& getInstance();

	//Recognition
	std::vector<glm::dvec2>			footprintToVector(Footprint* footprint_);
	std::vector<Result*>			recognize(std::vector<glm::dvec2>& input_, double beta_ = DEFAULT_BETA, double lambda_ = DEFAULT_LAMBDA, double kappa_ = DEFAULT_KAPPA, double e_sigma_ = DEFAULT_E_SIGMA);

	//Pattern Creation
	void getPatternsFromFile();
	void createPointsFromPatterns();

	//Debug
	std::vector<glm::dvec2>			searchPatternsDebug(std::vector<glm::dvec2> points_, int min_, int max_);
	char*							displayBigValueDebug(double value, int digits, int numeric);
	void							rotatePattern(Pattern* pattern_, double angle_);

// ----------------------------------------------------------------------------------------------------------------
private://											PRIVATE
// ----------------------------------------------------------------------------------------------------------------

	static PatternRecognition* _instance;

	// -------------------------------- METHODS --------------------------------
	PatternRecognition() {}

	//Tools Recognition
	std::vector<IncrementalResult*>	getIncrementalResults(std::vector<glm::dvec2>& input_, double beta_, double lambda_, double kappa_, double e_sigma_);
	IncrementalResult*		getIncrementalResult(std::vector<glm::dvec2>& unkPts_, Pattern* pattern_, double beta_, double lambda_, double e_sigma_);
	double					getLikelihoodOfMatch(std::vector<glm::dvec2>& pts1_, std::vector<glm::dvec2>& pts2_, double eSigma_, double aSigma_, double lambda_);
	double					getEuclidianDistance(std::vector<glm::dvec2>& pts1_, std::vector<glm::dvec2>& pts2_);
	double					getTurningAngleDistance(std::vector<glm::dvec2>& pts1_, std::vector<glm::dvec2>& pts2_);
	double					getTurningAngleDistance(glm::dvec2& ptA1_, glm::dvec2& ptA2_, glm::dvec2& ptB1_, glm::dvec2& ptB2_);
	void					marginalizeIncrementalResults(std::vector<IncrementalResult*>& results);
	std::vector<Result*>	getResults(std::vector<IncrementalResult*>& incrementalResults_);

	//Tools Patterns
	void				resample(std::vector<glm::dvec2>& list_, std::vector<glm::dvec2>& listOUT_, size_t countTargetPoints_);
	void				normalize(std::vector<glm::dvec2>& list_);
	
	void				scaleTo(std::vector<glm::dvec2>& list_, double sizeOfNormalizedSpace_);
	void				scale(std::vector<glm::dvec2>& list_, double scaleX_, double scaleY_, double originX_, double originY_);
	void				scale(std::vector<glm::dvec2>& list_, double scaleX_, double scaleY_);
	void				translate(std::vector<glm::dvec2>& list_, double offsetX_, double offsetY_);

	int					getResamplingPointCount(std::vector<glm::dvec2>& list_, float spacing_between_points_);
	double				getSpatialLength(std::vector<glm::dvec2>& list_);
	double				getSegmentPoints(std::vector<glm::dvec2>& list_, std::vector<int>& segmentPoints_, double length_);
	glm::dvec2			getCentroid(std::vector<glm::dvec2>& list_);
	glm::dvec4			getBoundingBox(std::vector<glm::dvec2>& list_);
};

#endif //PATTERN_RECOGNITION_H