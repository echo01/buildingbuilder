#include "Footprint.h"
#include <Agmd3D/Core/Model/SceneMgr.h>
#include <Core/SceneNode/MeshNode.h>
#include <map>

Footprint::Footprint() : _arcMaxAngle(28.65), _arcMinAngle(5.8), _pillarMaxDepth(0.5), _balconyMaxDepth(2), _arcResolution(6), _windowMin(1.2f), _windowMax(1.5f), _roofInset(.1f), _roofHeight(2.f)
{
}

Footprint::Footprint(double arcMaxAngle, double arcMinAngle) : _arcMaxAngle(arcMinAngle), _arcMinAngle(arcMinAngle)
{
}

Footprint::~Footprint() {
	/*
	for (auto i = 0; i < this->_recognizedEdges.size(); ++i) {
		delete this->_recognizedEdges[i]._shape;
	}
	*/
	delete this->_recognitionNode;
	delete this->_buildingNode;
	delete this->_footprintNode;

	for (int i = this->_floors.size() - 1; i >= 0; --i) {
		delete this->_floors[i];
	}
}

void Footprint::regenerateBuilding() {
	delete this->_buildingNode;
	this->generateBuilding();
}

void Footprint::reset(Agmd::SceneMgr* scene) {
	this->_edgesToRecognize.clear();
	this->_outputPoly.clear();
	this->_recognizedEdges.clear();

	scene->RemoveNode(this->_recognitionNode);
	delete this->_recognitionNode;

	scene->RemoveNode(this->_buildingNode);
	delete this->_buildingNode;
}

glm::mat4 generateLocalMatrix(const glm::vec3& pos, const glm::dvec2& direction, const glm::dvec2& normal, float height) {
	glm::mat4 localMatrix;

	glm::vec3 dir = glm::vec3(direction, 0.f);
	glm::vec3 nor = glm::vec3(normal, 0.f);
	glm::vec3 cross = glm::normalize(glm::cross(nor, dir)) * height;

	localMatrix[0] = glm::vec4(dir, 0.f);
	localMatrix[1] = glm::vec4(nor, 0.f);
	localMatrix[2] = glm::vec4(cross, 0.f);
	localMatrix[3] = glm::vec4(pos, 1.f);

	return localMatrix;
}


Agmd::Model* createMeshFromOutput(Footprint* footprint) {
	std::vector<Agmd::Model::TVertex> vertices;
	std::vector<Agmd::Model::TIndex> indices;

	int size = footprint->_outputPoly.size();
	for (int i = 0; i < size; ++i) {
		FootprintEdge edge = footprint->_outputPoly[i];
		Agmd::Model::TVertex v1;
		Agmd::Model::TVertex v2;

		v1.position = vec3(edge._v1, 0);
		v1.position.z = 0;
		v1.normal = vec3(edge._v1, 0);
		v1.color = Color::red.ToABGR();
		vertices.push_back(v1);

		v2.position = vec3(edge._v2, 0);
		v2.position.z = 0;
		v2.normal = vec3(edge._v2, 0);
		v2.color = Color::red.ToABGR();
		vertices.push_back(v2);

		indices.push_back(i * 2);
		indices.push_back(i * 2 + 1);
	}

	return new Agmd::Model(&vertices[0], vertices.size(), &indices[0], indices.size(), Agmd::PT_LINELIST);
}

Agmd::Model* createMeshFromPoly(Footprint* fp)
{
	std::vector<Agmd::Model::TVertex> vertices;
	std::vector<Agmd::Model::TIndex> indices;
	const  std::vector<FootprintNode>& outer = fp->_outerPolygon;
	int firstIndex = 0;
	for (a_uint32 i = 0, len = outer.size(); i < len; i++)
	{
		firstIndex = 0;
		Agmd::Model::TVertex vertex;
		vertex.position = vec3(outer[i]._position, 0);
		vertex.position.z = 0;
		vertex.normal = vec3(outer[i]._lonlat, 0);
		vertex.color = Color::white.ToABGR();
		vertices.push_back(vertex);
		if (i + 1 == len)
		{
			indices.push_back(i);
			indices.push_back(firstIndex);
		}
		else
		{
			indices.push_back(i);
			indices.push_back(i + 1);
		}
	}

	return new Agmd::Model(&vertices[0], vertices.size(), &indices[0], indices.size(), Agmd::PT_LINELIST);
};


void printVec(glm::dvec2 v) {
	std::cout << "(" << v.x << ", " << v.y << ")";
}

bool isPolygonCCW(const std::vector<glm::dvec2>& poly) {
	int size = poly.size();
	if (size < 3) return true;
	float angle = 0.f;
	for (auto i = 0; i < size - 2; ++i) {
		angle += glm::orientedAngle(glm::normalize(poly[i + 1] - poly[i]), glm::normalize(poly[i + 2] - poly[i + 1]));
	}
	return angle >= 0.f;
}

void Footprint::generateFootprintNode() {
	this->_footprintNode = new Agmd::MeshNode(createMeshFromPoly(this));
}

void Footprint::setOuterPolygon(const std::vector<glm::dvec2>& poly) {

	if (isPolygonCCW(poly)) {
		for (auto i = 0; i < poly.size(); ++i) {
			this->_outerPolygon.push_back(FootprintNode(poly[i]));
		}
	}
	else {
		for (int i = poly.size() - 1; i >= 0; --i) {
			this->_outerPolygon.push_back(FootprintNode(poly[i]));
		}
	}
}

void Footprint::recognizeArcs() {

	std::vector<a_uint32> recognizedEdges;

	//---------------
	//get arcs
	glm::dvec2 lastDirection(0, 1);
	double lastAngle = 0;
	std::vector<glm::dvec2> arcs;

	for (a_uint32 i = 0; i < _edges.size(); ++i) {
		const glm::dvec2& direction = _edges[i]._direction;
		double angle = glm::angle(lastDirection, direction);

		if((angle < -this->_arcMinAngle || angle > this->_arcMinAngle) && (angle > -this->_arcMaxAngle && angle < this->_arcMaxAngle)) {
			if (arcs.size() != 0 && arcs.back().y + 1 == i) {
				++arcs.back().y;
				recognizedEdges.push_back(i);
			} else {
				arcs.push_back(glm::dvec2( (i == 0) ? this->_outerPolygon.size() - 1 : i - 1, i));
				recognizedEdges.push_back(i);
			}
		}

		lastDirection = direction;
		lastAngle = angle;
	}

	for (std::vector<glm::dvec2>::iterator it = arcs.begin(); it != arcs.end(); ++it) {
		
	}

	//---------------------------
	//get center and radius
	std::vector<glm::dvec2>::iterator e = arcs.end();
	for (std::vector<glm::dvec2>::iterator it = arcs.begin(); it != e; ++it) {
		Arc* arc = new Arc;
		int edgeCount = it->y - it->x;
		glm::dvec2 meanCenter(0);
		double meanRadius = 0;

		if (it->x > _edges.size() - 1) {
			it->x = _edges.size() - 1;
		}
		else {
			recognizedEdges.push_back(it->x);
		}
		if (it->y > _edges.size() - 1) {
			it->y = _edges.size() - 1;
		}

		//find normals intersections
		for (int i = it->x; i < it->y; ++i) {
			for (int j = it->x; j < it->y; ++j) {
				if (i != j) {
					double x = (_edges[j]._normalEquation.y - _edges[i]._normalEquation.y) / (_edges[i]._normalEquation.x - _edges[j]._normalEquation.x);
					double y = _edges[i]._normalEquation.x * x + _edges[i]._normalEquation.y;
					meanCenter += glm::dvec2(x, y);
				}
			}
		}

		meanCenter /= edgeCount * (edgeCount - 1);

		//get radius
		for (int i = it->x; i < it->y; ++i) {
			meanRadius += glm::length(_edges[i]._v1 - meanCenter);
		}
		meanRadius /= edgeCount;

		glm::dvec2 v1 = glm::normalize(_edges[it->x]._v1 - meanCenter);
		glm::dvec2 v2 = glm::normalize(_edges[it->y]._v2 - meanCenter);

		arc->center = meanCenter;
		arc->resolution = edgeCount;
		arc->radius = meanRadius;
		arc->angle = glm::orientedAngle(v1, v2);

		v1 = _edges[it->x]._v1;
		v2 = _edges[it->y]._v2;

		_recognizedEdges[it->x] = RecognizedEdge(v1, v2, arc);
	}

	updateEdgesToRecognize(recognizedEdges);
}

bool isPerpendicular(const FootprintEdge& e1, const FootprintEdge& e2) {
	double angle = glm::orientedAngle(e1._direction, e2._direction);

	return angle > 80 && angle < 100;
}

bool isPillar(double pillarMaxDepth, const FootprintEdge& e1, const FootprintEdge& e2, const FootprintEdge& e3) {
	if (e1._magnitude > pillarMaxDepth || e3._magnitude > pillarMaxDepth)
		return false;

	return isPerpendicular(e1, e2) && isPerpendicular(e2, e3);
}

void Footprint::recognizeExtrusions()
{
	std::vector<a_uint32> recognizedEdges;
	const float maxAngle = 5;

	a_uint32 recoCount = _edgesToRecognize.size() - 1;
	a_uint32 edgeCount = _edges.size() - 1;
	for (a_uint32 i = 0; i < _edgesToRecognize.size(); ++i) {
		int index = _edgesToRecognize[i];
		
		a_uint32 e1 = index % edgeCount;
		a_uint32 e2 = (index + 1) % edgeCount;
		a_uint32 e3 = (index + 2) % edgeCount;

		if(isPillar(_pillarMaxDepth, _edges[e1], _edges[e2], _edges[e3])) 
		{
			Pillar* pillar = new Pillar;
			pillar->depth = _edges[e1]._magnitude;
			_recognizedEdges[e1] = RecognizedEdge(_edges[e1]._v1, _edges[e3]._v2, pillar);

			recognizedEdges.push_back(i);
			recognizedEdges.push_back(i + 1 % recoCount);
			recognizedEdges.push_back(i + 2 % recoCount);
		}
	}

	updateEdgesToRecognize(recognizedEdges);
}

void Footprint::naiveRecognition()
{
	this->_edges.clear();
	this->_edgesToRecognize.clear();
	this->_recognizedEdges.clear();
	this->_outputPoly.clear();

	//calculate edges direction, normals, normals equations (ax+ b)
	int size = _outerPolygon.size() - 1;
	for (int i = 0; i < size; ++i) {
		_edges.push_back(FootprintEdge(_outerPolygon[i]._position, _outerPolygon[i+1]._position, i, i+1));
		_edgesToRecognize.push_back(i);
	}
	//_edges.push_back(FootprintEdge(_outerPolygon.back()._position, _outerPolygon.front()._position, _outerPolygon.size() - 1, 0));
	//_edgesToRecognize.push_back(size);

	//recognize arcs
	this->recognizeArcs();
	this->recognizeExtrusions();

	for (auto i = 0; i < this->_edgesToRecognize.size(); ++i) {
		a_uint32 e = this->_edgesToRecognize[i];
		Wall* wall = new Wall();
		_recognizedEdges[e] = RecognizedEdge(_edges[e]._v1, _edges[e]._v2, wall);
	}

	this->processOutputPoly();

	if (this->_outputPoly.size() > 0) {
		this->_recognitionNode = new Agmd::MeshNode(createMeshFromOutput(this));
	}
	else{
		this->_recognitionNode = new Agmd::SceneNode();
	}
}

bool sortDesc(a_uint32 a, a_uint32 b) {
	return a > b;
}

void Footprint::updateEdgesToRecognize(std::vector<a_uint32>& recognizedEdges)
{
	a_uint32 before = _edgesToRecognize.size();
	std::sort(recognizedEdges.begin(), recognizedEdges.end(), sortDesc);
	std::vector<a_uint32>::iterator begin = _edgesToRecognize.begin();
	for(int i = 0; i < recognizedEdges.size(); ++i) {
		_edgesToRecognize.erase(begin + recognizedEdges[i]);
	}
}

void Footprint::processOutputPoly()
{
	int size = _recognizedEdges.size();
	std::map<a_uint32, RecognizedEdge>::const_iterator
		it(_recognizedEdges.begin()),
		end(_recognizedEdges.end());
	for (; it != end; ++it) {
		RecognizedEdge recognizedEdge = it->second;
		switch (recognizedEdge._shape->getType()) {
		case PILLAR:
		{
			FootprintEdge edge(recognizedEdge._v1, recognizedEdge._v2, 0, 0);
			edge._shape = recognizedEdge._shape;
			_outputPoly.push_back(edge);
		}
			break;
		case ARC:
		{
			Arc* arc = (Arc*)recognizedEdge._shape;
			float resolution = _arcResolution; //float resolution = arc->resolution;
			glm::dvec2 center = arc->center;
			float radius = arc->radius;
			float angleStep = arc->angle / resolution;

			glm::dvec2 unit(1, 0);
			glm::dvec2 firstAngle = recognizedEdge._v1 - arc->center;
			firstAngle = glm::normalize(firstAngle);

			float currentAngle = glm::orientedAngle(unit, firstAngle);
			glm::dvec2 v1 = recognizedEdge._v1;
			for (int j = 0; j < resolution; ++j) {
				currentAngle += angleStep;
				glm::dvec2 v2(cos(D_R * currentAngle), sin(D_R * currentAngle));
				v2 *= radius;
				v2 += center;
				FootprintEdge edge = FootprintEdge(v1, v2, 0, 0);
				edge._shape = recognizedEdge._shape;
				_outputPoly.push_back(edge);
				v1 = v2;
			}
		}
			break;
		case WALL:
			//create subwalls
			glm::dvec2 direction = recognizedEdge._v2 - recognizedEdge._v1;
			int nbWalls = (int)(glm::length(direction) / _windowMax);
			if (nbWalls == 0) nbWalls = 1;
			glm::dvec2 wallVec = direction / (double)nbWalls;

			for (int i = 0; i < nbWalls; ++i) {
				glm::dvec2 v1 = recognizedEdge._v1 + wallVec * (double)i;
				glm::dvec2 v2 = recognizedEdge._v1 + wallVec * (double)(i + 1);
				FootprintEdge edge(v1, v2, 0, 0);
				edge._shape = recognizedEdge._shape;
				_outputPoly.push_back(edge);
			}
		}
	}
	// compute corners
	size = _outputPoly.size() - 1;
	_outputPoly[0]._cornerBegin = glm::normalize(_outputPoly[size]._normal + _outputPoly[0]._normal);
	_outputPoly[0]._cornerEnd = glm::normalize(_outputPoly[0]._normal + _outputPoly[1]._normal);
	for (a_uint32 i = 1; i < size; ++i) {
		_outputPoly[i]._cornerBegin = glm::normalize(_outputPoly[i - 1]._normal + _outputPoly[i]._normal);
		_outputPoly[i]._cornerEnd = glm::normalize(_outputPoly[i]._normal + _outputPoly[i + 1]._normal);
	}
	_outputPoly[size]._cornerBegin = glm::normalize(_outputPoly[size]._normal + _outputPoly[size - 1]._normal);
	_outputPoly[size]._cornerEnd = glm::normalize(_outputPoly[size]._normal + _outputPoly[0]._normal);
}

Agmd::SceneNode* createNodeFromMesh(Agmd::Mesh* mesh) {
	return new Agmd::MeshNode(new Agmd::Model(&mesh->vertices[0], mesh->vertices.size(), &mesh->indices[0], mesh->indices.size()));
}

void createPillar(FootprintEdge edge, Agmd::SceneNode* node, float height, float baseHeight, Agmd::Mesh* mesh) {
	glm::dvec2 dir = glm::normalize(edge._v2 - edge._v1);
	glm::dvec2 normal = edge._normal;
	float scale = glm::length(edge._v2 - edge._v1);
	glm::dvec2 pos = edge._v1 + (edge._v2 - edge._v1) / glm::dvec2(2.f);
	glm::vec3 position(pos, baseHeight);
	glm::mat4 localMatrix = generateLocalMatrix(position, dir * glm::dvec2(scale), normal, height);

	Agmd::AssetLoader assetLoader;
	Agmd::SceneNode* model = createNodeFromMesh(mesh);
	model->getTransform().setLocalModelMatrix(localMatrix);
	node->addChild(model);
}

Agmd::MeshNode* findMesh(Agmd::SceneNode* node) {
	if (node->getType() == Agmd::NodeType::MESH_NODE) {
		return (Agmd::MeshNode*)(node);
	}
	auto children = node->GetChilden();
	for (auto i = 0; i < children.size(); ++i) {
		return findMesh(children[i]);
	}
}

void createWall(FootprintEdge edge, Agmd::SceneNode* node, float height, float baseHeight, float normalscale, const Agmd::Mesh* mesh) {
	// create local matrix
	glm::dvec2 dir = glm::normalize(edge._v2 - edge._v1);
	glm::dvec2 normal = edge._normal * (double)normalscale;//glm::dvec2(dir.y, -dir.x);
	float scale = glm::length(edge._v2 - edge._v1);
	glm::dvec2 pos = edge._v1 + (edge._v2 - edge._v1) / glm::dvec2(2.f);
	glm::vec3 position(pos, baseHeight);
	glm::mat4 localMatrix = generateLocalMatrix(position, dir * glm::dvec2(scale), normal, height);

	// load and instanciate mesh
	Agmd::Mesh m;

	// corner connection
	glm::mat2 matrix = glm::mat2(glm::inverse(localMatrix));
	glm::vec2 cornerBegin = matrix * glm::vec2(edge._cornerBegin);
	glm::vec2 cornerEnd = matrix * glm::vec2(edge._cornerEnd);

	for (auto i = 0; i < mesh->vertices.size(); ++i) {
		Agmd::Model::TVertex v(mesh->vertices[i]);
		if (v.position.x == .5f) {
			v.position.x += v.position.y * cornerEnd.x / cornerEnd.y;
		}
		else if (v.position.x == -.5f) {
			v.position.x += v.position.y * cornerBegin.x / cornerBegin.y;
		}
		m.vertices.push_back(v);
	}

	for (auto i = 0; i < mesh->indices.size(); ++i) {
		m.indices.push_back(mesh->indices[i]);
	}

	Agmd::MeshNode* model = new Agmd::MeshNode(new Agmd::Model(&m.vertices[0], m.vertices.size(), &m.indices[0], m.indices.size()));
	
	if (mesh->material != NULL) {
		model->setMaterial(mesh->material);
	}
	
	model->getTransform().setLocalModelMatrix(localMatrix);

	node->addChild(model);
}

struct indexedVert {
	a_uint32 id;
	glm::vec3 pos;
};

bool isInside(Agmd::Mesh& mesh, a_uint32 i1, a_uint32 i2, a_uint32 i3) {
	const glm::vec3 up(0.f, 0.f, 1.f);

	for (auto i = 0; i < mesh.vertices.size(); ++i) {
		if (i != i1 && i != i2 && i != i3) {
			glm::vec3 v = mesh.vertices[i].position;

			float angle = glm::orientedAngle(
				glm::normalize(mesh.vertices[i1].position - v), glm::normalize(mesh.vertices[i2].position - v), up) +
				glm::orientedAngle(glm::normalize(mesh.vertices[i2].position - v), glm::normalize(mesh.vertices[i3].position - v), up) +
				glm::orientedAngle(glm::normalize(mesh.vertices[i3].position - v), glm::normalize(mesh.vertices[i1].position - v), up);

			if (angle > 180.f) {
				return true;
			}
		}
	}

	return false;
}

bool createTriangle(std::vector<indexedVert>& vertices, Agmd::Mesh& mesh) {
	auto last = vertices.size() - 1;

	for (auto i = 0; i < vertices.size(); ++i) {
		a_uint32 i1 = i == 0 ? last : i - 1;
		a_uint32 i2 = i;
		a_uint32 i3 = i == last ? 0 : i + 1;

		auto prev = vertices[i1];
		auto cur = vertices[i2];
		auto next = vertices[i3];

		if (glm::cross(prev.pos - cur.pos, next.pos - cur.pos).z >= 0.f) {
			continue;
		}
		
		if (isInside(mesh, prev.id, cur.id, next.id)) {
			continue;
		}

		mesh.indices.push_back(prev.id);
		mesh.indices.push_back(cur.id);
		mesh.indices.push_back(next.id);

		vertices.erase(vertices.begin() + i);
		
		return true;
	}

	return false;
}

void capPolygon(Agmd::Mesh& mesh) {
	
	std::vector<indexedVert> vertices;
	bool triangleMade = true;

	for (auto i = 0; i < mesh.vertices.size(); ++i) {
		indexedVert vert;
		vert.id = i;
		vert.pos = mesh.vertices[i].position;
		vertices.push_back(vert);
	}

	while (triangleMade) {
		triangleMade = createTriangle(vertices, mesh);
	}
}

void createRoof(Agmd::SceneNode* node, Footprint* footprint, float baseHeight, float roofHeight, float inset) {
	std::vector<Agmd::Model::TVertex> vertices;
	std::vector<Agmd::Model::TIndex> indices;
	Agmd::Mesh topMesh;
	const glm::vec3 up(0.f, 0.f, 1.f);
	float roofTop = baseHeight + roofHeight;

	// process faces
	for (auto i = 0; i < footprint->_outputPoly.size(); ++i) {
		FootprintEdge edge = footprint->_outputPoly[i];
		Agmd::Model::TVertex v1, v2, v3, v4, topVertex;

		// calcul de la normale
		glm::vec3 b = glm::normalize(up * roofHeight - glm::vec3(edge._normal, 0.f) * inset);
		glm::vec3 normal = glm::cross(glm::vec3(edge._direction, 0.f), b);

		// bottom
		v1.position = glm::vec3(edge._v1.x, edge._v1.y, baseHeight);
		v2.position = glm::vec3(edge._v2.x, edge._v2.y, baseHeight);
		v1.normal = normal;
		v2.normal = normal;

		// top
		glm::vec3 cornerBegin = glm::vec3(edge._cornerBegin, 0.f);
		glm::vec3 cornerEnd = glm::vec3(edge._cornerEnd, 0.f);
		v3.position = glm::vec3(edge._v1.x, edge._v1.y, roofTop) - cornerBegin * inset;
		v4.position = glm::vec3(edge._v2.x, edge._v2.y, roofTop) - cornerEnd * inset;
		v3.normal = normal;
		v4.normal = normal;
		topVertex.position = v3.position;
		topVertex.normal = up;
		topMesh.vertices.push_back(topVertex);

		// add vertices
		vertices.push_back(v1);
		vertices.push_back(v2);
		vertices.push_back(v3);
		vertices.push_back(v4);

		// add indices
		// first triangle
		indices.push_back(i * 4);
		indices.push_back(i * 4 + 1);
		indices.push_back(i * 4 + 3);

		// second triangle
		indices.push_back(i * 4);
		indices.push_back(i * 4 + 3);
		indices.push_back(i * 4 + 2);
	}

	Agmd::Model* model = new Agmd::Model(&vertices[0], vertices.size(), &indices[0], indices.size(), Agmd::PT_TRIANGLELIST);
	node->addChild(new Agmd::MeshNode(model));

	// cap top polygon
	capPolygon(topMesh);
	model = new Agmd::Model(&topMesh.vertices[0], topMesh.vertices.size(), &topMesh.indices[0], topMesh.indices.size(), Agmd::PT_TRIANGLELIST);
	node->addChild(new Agmd::MeshNode(model));
}

void Footprint::generateBuilding()
{
	Agmd::SceneNode* node = new Agmd::SceneNode();
	float floorBase = 0.f;
	float pillarBase = 0.f;
	int pillarLastFloor = -1;
	Agmd::Mesh* pillarModel;

	for (auto i = 0; i < this->_floors.size(); ++i) {
		Floor* floor = _floors[i];
		float floorHeight = floor->height;
		
		for (a_uint32 j = 0; j < this->_outputPoly.size(); ++j) {
			FootprintEdge edge = this->_outputPoly[j];
			switch (edge._shape->getType()) {
			case PILLAR:
				if (i == pillarLastFloor && i != 0) {
					createPillar(edge, node, floorBase + floorHeight - pillarBase, pillarBase, pillarModel);
				}
				break;
			case ARC:
				createWall(edge, node, floorHeight, floorBase, floor->wallScale, floor->arcMesh);
				break;
			case WALL:
				createWall(edge, node, floorHeight, floorBase, floor->wallScale, (edge._magnitude < _windowMin) ? floor->wallMesh : floor->windowMesh);
				break;
			}
		}

		if (i > pillarLastFloor) {
			pillarLastFloor = i + floor->pillarHeight - 1;
			pillarBase = floorBase;
			pillarModel = floor->pillarMesh;
		}

		floorBase += floorHeight;
	}

	createRoof(node, this, floorBase, _roofHeight, _roofInset);
	
	this->_buildingNode = node;
}

FootprintEdge::FootprintEdge(const glm::dvec2& v1, const glm::dvec2& v2, a_uint32 i1, a_uint32 i2) : _v1(v1), _v2(v2), _i1(i1), _i2(i2)
{
	glm::dvec2 diff = v2 - v1;
	this->_magnitude = glm::length(v2 - v1);
	this->_direction = glm::normalize(diff);
	this->_normal = glm::dvec2(this->_direction.y, -this->_direction.x);
	this->_center = glm::dvec2(diff.x / 2, diff.y / 2) + v1;
	this->_normalEquation.x = _normal.x == 0 ? std::numeric_limits<double>::infinity() : _normal.y / _normal.x;
	this->_normalEquation.y = this->_normalEquation.x == std::numeric_limits<double>::infinity() ?  std::numeric_limits<double>::infinity() : _center.y - _center.x * this->_normalEquation.x;
}