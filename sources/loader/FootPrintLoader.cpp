

#include "FootPrintLoader.h"
#include <AgmdUtilities/Utilities/TinyXml/tinyxml.h>

#include <string>
#include <list>
#include <stdlib.h>
#include <string.h>
#define _USE_MATH_DEFINES
#include <math.h>
#define RADIUS 6378137

double fmin(double a, double b) { return a < b ? a : b; };
double fmax(double a, double b) { return a > b ? a : b; };

glm::dvec2 fromGeographic(double lat, double lon, const glm::dvec2& center) {
	lat = radians(lat);
	lon = radians(lon - center.x);
	double B = sin(lon) * cos(lat);
	double x = 0.5 * RADIUS * log((1+B)/(1-B));
	double y = RADIUS * (atan(tan(lat)/cos(lon)) - radians(center.y));

	return glm::dvec2(x, y);
}

struct Node
{
	a_uint64 id;
	double lat;
	double lon;
	glm::dvec2 pos;
};

struct  Bounds
{
	dvec2 min;
	dvec2 max;
	dvec2 center;
};
struct Way
{
	a_uint64 id;
	std::vector<Node*> nodes;
	std::map<std::string, std::string> tags;
};
struct Relation
{
	a_uint64 id;
	std::vector<std::pair<std::string, Way*>> ways; //std::string is the way's role (inner or outer)
	std::map<std::string, std::string> tags;
};

void parsingOSM(TiXmlNode* pParent, std::map<a_uint64, Node>& _nodes, std::map<a_uint64, Way>& _ways, std::map<a_uint64, Relation>& _relations,Bounds& _bounds);

std::vector<Footprint*>* analyzeOsm(const std::string& filename)
{
	TiXmlDocument doc(filename.c_str());
	bool loadOkay = doc.LoadFile();
	std::map<a_uint64, Node> nodes;
	std::map<a_uint64, Way> ways;
	std::map<a_uint64, Relation> relations;

	Bounds _bounds;
	if (loadOkay)
		parsingOSM( &doc, nodes, ways, relations,_bounds);
	else
		printf("Failed to load file \"%s\"\n", filename.c_str());
	printf("[IMPORT_OSM] %d nodes || %d ways || %d relations\n", nodes.size(), ways.size(), relations.size());

	//IN RELATIONS
	std::vector<Relation*> buildingsRelation;
	int countBuldingsRelation = 0;

	std::map<a_uint64, Relation>::iterator itRel = relations.begin();
	std::map<a_uint64, Relation>::iterator endRel = relations.end();
	for(; itRel != endRel; ++itRel) {
		std::map<std::string, std::string>::iterator itTagsRel = itRel->second.tags.begin();
		std::map<std::string, std::string>::iterator endTagsRel = itRel->second.tags.end();
		for(; itTagsRel != endTagsRel; ++itTagsRel) {
			if (itTagsRel->first == "building" && itTagsRel->second == "yes") {
				++countBuldingsRelation;
				buildingsRelation.push_back(&(itRel->second));
			}
		}
	}
	std::cout << "[ANALYSE_OSM] FOUND " << countBuldingsRelation << " buildings in relations" << std::endl;

	//IN WAYS
	std::vector<Way*> buildingsWay;
	int countBuildingsWay = 0;

	std::map<a_uint64, Way>::iterator itWay = ways.begin();
	std::map<a_uint64, Way>::iterator endWay = ways.end();
	for(; itWay != endWay; ++itWay) {
		std::map<std::string, std::string>::iterator itTagsWay = itWay->second.tags.begin();
		std::map<std::string, std::string>::iterator endTagsWay = itWay->second.tags.end();
		for(; itTagsWay != endTagsWay; ++itTagsWay) {
			if (itTagsWay->first == "building" /*&& itTagsWay->second == "yes"*/) {
				++countBuildingsWay;
				buildingsWay.push_back(&(itWay->second));
			}
		}
	}


	std::vector<Footprint*>* footprints = new std::vector<Footprint*>();
	for(a_uint32 i =0, len = buildingsWay.size(); i < len; ++i)
	{
		Footprint* fp = new Footprint();
		std::vector<dvec2> points;
		Way *w =  buildingsWay[i];
		for(a_uint32 j =0, len2 = w->nodes.size(); j < len2; ++j)
			points.push_back(nodes[w->nodes[j]->id].pos);
		fp->setOuterPolygon(points);
		footprints->push_back(fp);
	}
	for(a_uint32 t =0, len3 = buildingsRelation.size(); t < len3; ++t)
	{
		Relation* r = buildingsRelation[t];
		for(a_uint32 i =0, len = r->ways.size(); i < len; ++i)
		{
			Footprint* fp = new Footprint();
			std::vector<dvec2> points;
			std::pair<std::string,Way*> way = r->ways[i];
			Way *w =  way.second;
			if(way.first.compare("inner") == 0)
				continue;
			for(a_uint32 j =0, len2 = w->nodes.size(); j < len2; ++j)
				points.push_back(nodes[w->nodes[j]->id].pos);
			fp->setOuterPolygon(points);
			footprints->push_back(fp);
		}
	}

	return footprints;
}

void parsingOSM(TiXmlNode* pParent, std::map<a_uint64, Node>& _nodes, std::map<a_uint64, Way>& _ways, std::map<a_uint64, Relation>& _relations, Bounds& _bounds)
{
	if ( !pParent ) return;

	TiXmlNode* pChild;
	int t = pParent->Type();

	switch ( t )
	{
	case TiXmlNode::TINYXML_DECLARATION:
	case TiXmlNode::TINYXML_DOCUMENT:
		for ( pChild = pParent->FirstChild(); pChild != 0; pChild = pChild->NextSibling()) 
		{
			parsingOSM( pChild, _nodes, _ways, _relations,_bounds);
		}
		break;

	case TiXmlNode::TINYXML_ELEMENT:
		if (strcmp(pParent->Value(), "osm") == 0)
		{
			for ( pChild = pParent->FirstChild(); pChild != 0; pChild = pChild->NextSibling()) 
			{
				parsingOSM( pChild, _nodes, _ways, _relations,_bounds);
			}
		}
		//--------------------------------
		// NEW NODE
		//---------------------------------
		else if (strcmp(pParent->Value(), "node") == 0) //Node (ignore tags in it)
		{
			Node newNode;
			TiXmlAttribute* pAttrib=pParent->ToElement()->FirstAttribute();
			while (pAttrib)
			{

				dvec2 len  = _bounds.max - _bounds.min;
				double _min = min(len.x,len.y);
				//pos /= _min/100;
				if (strcmp(pAttrib->Name(), "id") == 0)
					newNode.id = _strtoui64(pAttrib->Value(),NULL,10);//atoi(pAttrib->Value());
				else if (strcmp(pAttrib->Name(), "lat") == 0) {
					newNode.lat = atof(pAttrib->Value());
					//newNode.y = merc_y(newNode.lat-_bounds.center.y);
				}
				else if (strcmp(pAttrib->Name(), "lon") == 0) {
					newNode.lon = atof(pAttrib->Value());
					//newNode.x = merc_x(newNode.lon-_bounds.center.x);
				}
				pAttrib=pAttrib->Next();
			}	
#ifdef DEBUG_IMPORT_OSM
			printf("NEW NODE : id = %d, lat = %f, lon = %f", newNode.id, newNode.lon, newNode.lat);
#endif //DEBUG_IMPORT_OSM
			newNode.pos = fromGeographic(newNode.lat, newNode.lon, _bounds.center);
			_nodes[newNode.id] = newNode;
		}
		//--------------------------------
		// NEW WAY
		//---------------------------------
		else if (strcmp(pParent->Value(), "way") == 0) //New Way
		{
			Way newWay;
			TiXmlAttribute* pAttrib=pParent->ToElement()->FirstAttribute();
			while (pAttrib)
			{
				if (strcmp(pAttrib->Name(), "id") == 0) {

					newWay.id = _strtoui64(pAttrib->Value(),NULL,10);
					pAttrib=NULL;
				} else
					pAttrib=pAttrib->Next();
			}

			//Adding nodes and tags to the way
			int countNodes = 0;
			int countTags = 0;
			for ( pChild = pParent->FirstChild(); pChild != 0; pChild = pChild->NextSibling()) 
			{
				int typeChild = pChild->Type();
				const char* valueChild = pChild->Value();

				//Addind nodes to the way
				if (typeChild == TiXmlNode::TINYXML_ELEMENT && strcmp(valueChild, "nd") == 0) 
				{
					TiXmlAttribute* pAttribChild = pChild->ToElement()->FirstAttribute();
					while (pAttribChild)
					{
						if (strcmp(pAttribChild->Name(), "ref") == 0) {
							const char* cvalue = pAttribChild->Value();
							a_uint64 value = _strtoui64(cvalue,NULL,10);
							newWay.nodes.push_back(&_nodes[value]);
							++countNodes;
							pAttribChild = NULL;
						} else
							pAttribChild = pAttribChild->Next();
					}
				}//Adding tags to the way
				else if (typeChild == TiXmlNode::TINYXML_ELEMENT && strcmp(valueChild, "tag") == 0)
				{
					std::string k = "";
					std::string v = "";
					TiXmlAttribute* pAttribChild = pChild->ToElement()->FirstAttribute();
					while (pAttribChild)
					{
						if (strcmp(pAttribChild->Name(), "k") == 0)
							k = pAttribChild->Value();
						else if (strcmp(pAttribChild->Name(), "v") == 0)
							v = pAttribChild->Value();

						pAttribChild = pAttribChild->Next();
					}
					//if (k != "" && v != "") 
					{
						newWay.tags[k] = v;
						++countTags;
					}
				}
			}
#ifdef DEBUG_IMPORT_OSM
			printf("NEW WAY : id = %d with %d child nodes and %d tags", newWay.id, countNodes, countTags);
#endif //DEBUG_IMPORT_OSM
			_ways[newWay.id] = newWay;
		}
		//--------------------------------
		// NEW RELATION
		//---------------------------------
		else if (strcmp(pParent->Value(), "relation") == 0) //New Relation
		{
			Relation newRelation;
			TiXmlAttribute* pAttrib=pParent->ToElement()->FirstAttribute();
			while (pAttrib)
			{
				if (strcmp(pAttrib->Name(), "id") == 0) {

					newRelation.id = _strtoui64(pAttrib->Value(),NULL,10);;
					pAttrib=NULL;
				} else
					pAttrib=pAttrib->Next();
			}

			//Addind ways and tags to the relation
			int countWays = 0;
			int countTags = 0;
			for ( pChild = pParent->FirstChild(); pChild != 0; pChild = pChild->NextSibling()) 
			{
				int typeChild = pChild->Type();
				const char* valueChild = pChild->Value();

				//Addind ways to the relation
				if (typeChild == TiXmlNode::TINYXML_ELEMENT && strcmp(valueChild, "member") == 0) 
				{
					TiXmlAttribute* pAttribChild = pChild->ToElement()->FirstAttribute();
					std::string role = "";
					Way* childWay;
					while (pAttribChild)
					{
						if (strcmp(pAttribChild->Name(), "ref") == 0)
							childWay = &_ways[_strtoui64(pAttribChild->Value(),NULL,10)];
						else if (strcmp(pAttribChild->Name(), "role") == 0)
							role = pAttribChild->Value();

						pAttribChild = pAttribChild->Next();
					}

					newRelation.ways.push_back(std::pair<std::string, Way*>(role, childWay));
					++countWays;

				} //Addind tags to the relation
				else if (typeChild == TiXmlNode::TINYXML_ELEMENT && strcmp(valueChild, "tag") == 0)
				{
					std::string k = "";
					std::string v = "";
					TiXmlAttribute* pAttribChild = pChild->ToElement()->FirstAttribute();
					while (pAttribChild)
					{
						if (strcmp(pAttribChild->Name(), "k") == 0)
							k = pAttribChild->Value();
						else if (strcmp(pAttribChild->Name(), "v") == 0)
							v = pAttribChild->Value();

						pAttribChild = pAttribChild->Next();
					}
					//if (k != "" && v != "") 
					{
						newRelation.tags[k] = v;
						++countTags;
					}
				}
			}
#ifdef DEBUG_IMPORT_OSM
			printf("NEW RELATION : id = %d with %d child ways and %d tags", newRelation.id, countWays, countTags);
#endif //DEBUG_IMPORT_OSM

			_relations[newRelation.id] = newRelation;
		}else if (strcmp(pParent->Value(), "bounds") == 0) {
			TiXmlAttribute* pAttrib=pParent->ToElement()->FirstAttribute();
			while (pAttrib)
			{
				if (strcmp(pAttrib->Name(), "minlat") == 0)
					_bounds.min.y = (atof(pAttrib->Value()));
				else if (strcmp(pAttrib->Name(), "minlon") == 0)
					_bounds.min.x = (atof(pAttrib->Value()));
				else if (strcmp(pAttrib->Name(), "maxlat") == 0)
					_bounds.max.y = (atof(pAttrib->Value()));
				else if (strcmp(pAttrib->Name(), "maxlon") == 0)
					_bounds.max.x = (atof(pAttrib->Value()));
				pAttrib=pAttrib->Next();

			}
			_bounds.center = _bounds.min + (_bounds.max-_bounds.min)/2.0;
		}
		break;

	default:
		break;
	}
#ifdef DEBUG_IMPORT_OSM
	printf( "\n" );
#endif //DEBUG_IMPORT_OSM
}



FootPrintLoader::FootPrintLoader()
{

}

std::vector<Footprint*>* FootPrintLoader::LoadFromFile( const std::string& filename )
{
	return analyzeOsm(filename);
}

void FootPrintLoader::SaveToFile( const Footprint* object, const std::string& filename )
{

}
