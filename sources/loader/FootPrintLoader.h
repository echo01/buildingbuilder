/*
============================================================================
Demo - A test application !

Author : 
Cyril Basset (basset.cyril@gmail.com - https://github.com/Agamand)
Jean-Vincent Lamberti (https://github.com/Kinroux)

https://github.com/Agamand/AgmdEngine
status : in pause
============================================================================
*/

#ifndef FOOTPRINTLOADER_H
#define FOOTPRINTLOADER_H

#include <Core/Loader.h>
#include <Core/Model/Model.h>
#include <Core/Enums.h>
#include <istream>
#include "../geometryprocessing/FootPrint.h"
#include <vector>
using namespace Agmd;

class FootPrintLoader : public Loader<std::vector<Footprint*>>
{
public :

	FootPrintLoader();

	virtual std::vector<Footprint*>* LoadFromFile(const std::string& filename);
	virtual void SaveToFile(const Footprint* object, const std::string& filename);

private :
	static void OnError();
};



#endif /* FOOTPRINTLOADER_H */