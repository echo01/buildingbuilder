#include "OsmBuilderWindow.h"
#include <wx/choice.h>
#include <sstream>
#include "app.h"
#include <wx/filedlg.h>

const char* nodeStringType[] = {
	"Root",
	"DisplayNode",
	"Light",
	"Mesh",
	"CameraNode",
	"TreeNode"
};
const enum {
	NUMBER_FLOORS,
	MAX_ARC_ANGLE,
	MIN_ARC_ANGLE,
	BALCONY_MAX_DEPTH,
	PILLAR_MAX_DEPTH,
	ARC_RESOLUTION,
	ROOF_INSET,
	ROOF_HEIGHT,
	WINDOW_MIN,
	WINDOW_MAX,
	WALL_SCALE,
	FLOOR_HEIGHT,
	PILLAR_HEIGHT,
	PILLAR_MESH_NAME,
	WALL_MESH_NAME,
	WINDOW_MESH_NAME,
	ARC_MESH_NAME,
};
const char* propertyString[] = {
	"Etages",
	"Angle maximum",
	"Angle minimum",
	"Profondeur d'un balcon",
	"Profondeur d'un pilier",
	"R�solution des arcs",
	"Renfoncement du toit",
	"Hauteur du toit",
	"Largeur min d'une fen�tre",
	"Largeur max d'une fen�tre",
	"Profondeur des murs",
	"Hauteur de l'�tage",
	"Etages du pilier",
	"Mod�le de pilier",
	"Mod�le de mur",
	"Mod�le de fen�tre",
	"Mod�le de l'arc",
};

void OsmBuilderWindow::updateProperties(Footprint* footprint_, int numFloor_)
{
	m_lastFootprintSelected = footprint_;
	m_lastFloorSelected = numFloor_;

	m_properties->Clear();

	if (numFloor_ == -1) 
	{
		wxFloatProperty* countFloor = new wxFloatProperty(propertyString[NUMBER_FLOORS], wxPG_LABEL, footprint_->_floors.size());
		countFloor->Enable(false);
		m_properties->Append(countFloor);
		m_properties->Append(new wxPropertyCategory("Reconnaissance des arcs"));
		m_properties->Append(new wxFloatProperty(propertyString[MAX_ARC_ANGLE], wxPG_LABEL, footprint_->_arcMaxAngle));
		m_properties->Append(new wxFloatProperty(propertyString[MIN_ARC_ANGLE], wxPG_LABEL, footprint_->_arcMinAngle));
		m_properties->Append(new wxPropertyCategory("Reconnaissance des extrusions"));
		m_properties->Append(new wxFloatProperty(propertyString[BALCONY_MAX_DEPTH], wxPG_LABEL, footprint_->_balconyMaxDepth));
		m_properties->Append(new wxFloatProperty(propertyString[PILLAR_MAX_DEPTH], wxPG_LABEL, footprint_->_pillarMaxDepth));
		m_properties->Append(new wxPropertyCategory("G�n�ration"));
		m_properties->Append(new wxIntProperty(propertyString[ARC_RESOLUTION], wxPG_LABEL, footprint_->_arcResolution));
		m_properties->Append(new wxFloatProperty(propertyString[ROOF_INSET], wxPG_LABEL, footprint_->_roofInset));
		m_properties->Append(new wxFloatProperty(propertyString[ROOF_HEIGHT], wxPG_LABEL, footprint_->_roofHeight));
		m_properties->Append(new wxFloatProperty(propertyString[WINDOW_MIN], wxPG_LABEL, footprint_->_windowMin));
		m_properties->Append(new wxFloatProperty(propertyString[WINDOW_MAX], wxPG_LABEL, footprint_->_windowMax));
	}
	else 
	{
		m_properties->Append(new wxFloatProperty(propertyString[FLOOR_HEIGHT], wxPG_LABEL, footprint_->_floors.at(numFloor_)->height));
		m_properties->Append(new wxFloatProperty(propertyString[WALL_SCALE], wxPG_LABEL, footprint_->_floors.at(numFloor_)->wallScale));

		if (footprint_->_floors.at(numFloor_)->pillarMesh != NULL) 
		{
			m_properties->Append(new wxFloatProperty(propertyString[PILLAR_HEIGHT], wxPG_LABEL, footprint_->_floors.at(numFloor_)->pillarHeight));
			addEnumProperty(&(App::getPillarStyle()), footprint_->_floors.at(numFloor_)->pillarMesh->name, propertyString[PILLAR_MESH_NAME], footprint_, numFloor_);
		}
		
		addEnumProperty(&(App::getWallStyle()), footprint_->_floors.at(numFloor_)->wallMesh->name, propertyString[WALL_MESH_NAME], footprint_, numFloor_);
		addEnumProperty(&(App::getWallStyle()), footprint_->_floors.at(numFloor_)->arcMesh->name, propertyString[ARC_MESH_NAME], footprint_, numFloor_);
		addEnumProperty(&(App::getWindowStyle()), footprint_->_floors.at(numFloor_)->windowMesh->name, propertyString[WINDOW_MESH_NAME], footprint_, numFloor_);
	}
}
void OsmBuilderWindow::onClickAddFloor(wxCommandEvent& event)
{
	if (m_deleteOrAddIsRunning || m_lastFootprintSelected == NULL)
		return;
	m_deleteOrAddIsRunning = true;


	Floor* floor = new Floor;
	floor->height = m_lastFootprintSelected->_floors.back()->height;
	floor->pillarMesh = m_lastFootprintSelected->_floors.back()->pillarMesh;
	floor->windowMesh = m_lastFootprintSelected->_floors.back()->windowMesh;
	floor->wallMesh = m_lastFootprintSelected->_floors.back()->wallMesh;
	floor->arcMesh = m_lastFootprintSelected->_floors.back()->arcMesh;
	floor->pillarHeight = m_lastFootprintSelected->_floors.back()->pillarHeight;
	floor->wallScale = 2.0;
	m_lastFootprintSelected->_floors.push_back(floor);

	regenarationLastFootprint();

	updateSceneTree();

	m_deleteOrAddIsRunning = false;
}
void OsmBuilderWindow::onClickRemoveFloor(wxCommandEvent& event)
{
	if (m_deleteOrAddIsRunning || m_lastFootprintSelected == NULL || m_lastFootprintSelected->_floors.size() <= 1)
		return;
	m_deleteOrAddIsRunning = true;

	m_lastFootprintSelected->_floors.pop_back();

	regenarationLastFootprint();

	updateSceneTree();

	m_deleteOrAddIsRunning = false;
}
void OsmBuilderWindow::updateSceneTree(std::vector<Footprint*>* footprints_)
{
	if (footprints_ != NULL) {
		m_footprints = footprints_;
	}

	m_sceneTree->DeleteAllItems();

	wxTreeItemId idRoot = m_sceneTree->AppendItem(m_sceneTree->GetRootItem(), "Racine");

	std::vector<Footprint*>::iterator it = m_footprints->begin();
	std::vector<Footprint*>::iterator end = m_footprints->end();
	for (int count = 0; it != end; ++it, ++count)
	{
		std::ostringstream s;
		s << "B�timent " << count;
		std::string name(s.str());

		m_nodes[*it] = m_sceneTree->AppendItem(idRoot, name);
		m_nodesId[m_nodes[*it]] = *it;

		wxTreeItemId idFloor = m_nodes[*it];

		for (int i = 0, size = (*it)->_floors.size(); i < size; ++i)
		{
			std::ostringstream s;
			s << "Etage " << (i+1);
			std::string name(s.str());
			m_nodesFloorId[m_sceneTree->AppendItem(idFloor, name)] = std::make_pair(i, *it);
			//(*it)->_floors.at(i)->
		}
	}

	m_sceneTree->ExpandAll();
}
void OsmBuilderWindow::onChangedProperties(wxPropertyGridEvent& event)
{
	wxPGProperty* property = event.GetProperty();
	std::string name = property->GetName();
	//std::string val = property->GetValueAsString();
	//std::cout << name << " : " << val << std::endl;

	if (name.compare(propertyString[NUMBER_FLOORS]) == 0) 
	{
		//nada
	}
	else if (name.compare(propertyString[MAX_ARC_ANGLE]) == 0)
	{
		m_lastFootprintSelected->_arcMaxAngle = property->GetValue();
	}
	else if (name.compare(propertyString[MIN_ARC_ANGLE]) == 0)
	{
		m_lastFootprintSelected->_arcMinAngle = property->GetValue();
	}
	else if (name.compare(propertyString[BALCONY_MAX_DEPTH]) == 0)
	{
		m_lastFootprintSelected->_balconyMaxDepth = property->GetValue();
	}
	else if (name.compare(propertyString[PILLAR_MAX_DEPTH]) == 0)
	{
		m_lastFootprintSelected->_pillarMaxDepth = property->GetValue();
	}
	else if (name.compare(propertyString[FLOOR_HEIGHT]) == 0)
	{
		m_lastFootprintSelected->_floors[m_lastFloorSelected]->height = (double)property->GetValue();
	}
	else if (name.compare(propertyString[PILLAR_HEIGHT]) == 0)
	{
		m_lastFootprintSelected->_floors[m_lastFloorSelected]->pillarHeight = (long)property->GetValue();
	}
	else if (name.compare(propertyString[PILLAR_MESH_NAME]) == 0)
	{
		int id = (long)property->GetValue();
		m_lastFootprintSelected->_floors.at(m_lastFloorSelected)->pillarMesh = m_library->at(id);
	}
	else if (name.compare(propertyString[WALL_MESH_NAME]) == 0)
	{
		int id = (long)property->GetValue();
		m_lastFootprintSelected->_floors[m_lastFloorSelected]->wallMesh = m_library->at(id);
	}
	else if (name.compare(propertyString[ARC_MESH_NAME]) == 0)
	{
		int id = (long)property->GetValue();
		m_lastFootprintSelected->_floors[m_lastFloorSelected]->arcMesh = m_library->at(id);
	}
	else if (name.compare(propertyString[WINDOW_MESH_NAME]) == 0)
	{
		int id = (long)property->GetValue();
		m_lastFootprintSelected->_floors[m_lastFloorSelected]->windowMesh = m_library->at(id);
	}
	else if (name.compare(propertyString[ARC_RESOLUTION]) == 0)
	{
		int value = (long)property->GetValue();
		m_lastFootprintSelected->_arcResolution = value;
	}
	else if (name.compare(propertyString[ROOF_INSET]) == 0)
	{
		m_lastFootprintSelected->_roofInset = (long)property->GetValue();
	}
	else if (name.compare(propertyString[ROOF_HEIGHT]) == 0)
	{
		m_lastFootprintSelected->_roofHeight = (long)property->GetValue();
	}
	else if (name.compare(propertyString[WINDOW_MIN]) == 0)
	{
		m_lastFootprintSelected->_windowMin = (long)property->GetValue();
	}
	else if (name.compare(propertyString[WINDOW_MAX]) == 0)
	{
		m_lastFootprintSelected->_windowMax = (long)property->GetValue();
	}
	else if (name.compare(propertyString[WALL_SCALE]) == 0)
	{
		m_lastFootprintSelected->_floors[m_lastFloorSelected]->wallScale = (long)property->GetValue();
	}

	regenarationLastFootprint();
}
void OsmBuilderWindow::regenarationLastFootprint()
{
	m_app->m_RecognitionParent->removeChild(m_lastFootprintSelected->_recognitionNode);
	m_lastFootprintSelected->naiveRecognition();
	m_app->m_RecognitionParent->addChild(m_lastFootprintSelected->_recognitionNode);

	m_buildingsParent->removeChild(m_lastFootprintSelected->_buildingNode);
	m_lastFootprintSelected->regenerateBuilding();
	m_buildingsParent->addChild(m_lastFootprintSelected->_buildingNode);
}

void OsmBuilderWindow::onSelect(wxTreeEvent& event)
{
	wxTreeItemId id = event.GetItem();

	
	std::map<wxTreeItemId, std::pair<int, Footprint*>>::iterator itf;
	//Floor
	if ((itf = m_nodesFloorId.find(id)) != m_nodesFloorId.end())
	{
		updateProperties(itf->second.second, itf->second.first);
	}

	//Footprint
	std::map<wxTreeItemId, Footprint*>::iterator it;
	if ((it = m_nodesId.find(id)) != m_nodesId.end())
	{
		updateProperties(it->second, -1);
	}
}

void OsmBuilderWindow::addEnumProperty(std::vector<int>* styles_, std::string nameSelected_, std::string label_, Footprint* footprint_, int numFloor_)
{
	wxArrayString arrNames;
	wxArrayInt arrValue;
	int selected = -1;
	for (int i = 0, size = styles_->size(); i < size; ++i){
		int id = styles_->at(i);
		arrNames.Add((*m_library)[id]->name);
		arrValue.Add(styles_->at(i));
		if (selected == -1 && (*m_library)[styles_->at(i)]->name.compare(nameSelected_) == 0)
			selected = styles_->at(i);
	}
	m_properties->Append(new wxEnumProperty(label_, wxPG_LABEL, arrNames, arrValue, selected));
}

void OsmBuilderWindow::onImportOsm(wxCommandEvent& event) {
	wxFileDialog
		openFileDialog(this, _("Ouvrir un fichier OSM"), "", "",
		"ficheirs OpenStreetMap (*.osm)|*.osm", wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if (openFileDialog.ShowModal() == wxID_CANCEL)
		return;     // the user changed idea...

	// proceed loading the file chosen by the user;
	// this can be done with e.g. wxWidgets input streams:
	wxFileInputStream input_stream(openFileDialog.GetPath());
	if (!input_stream.IsOk())
	{
		wxLogError("Cannot open file '%s'.", openFileDialog.GetPath());
		return;
	}
	m_app->LoadOsm(openFileDialog.GetPath());
}

void OsmBuilderWindow::onExport(wxCommandEvent& event) {
	wxFileDialog
		saveFileDialog(this, _("Export to obj"), "", "",
		"OBJ files (*.obj)|*.obj", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveFileDialog.ShowModal() == wxID_CANCEL)
		return;     // the user changed idea...

	// save the current contents in the file;
	// this can be done with e.g. wxWidgets output streams:
	wxFileOutputStream output_stream(saveFileDialog.GetPath());
	if (!output_stream.IsOk())
	{
		wxLogError("Cannot save current contents in file '%s'.", saveFileDialog.GetPath());
		return;
	}

	m_app->Export(saveFileDialog.GetPath());
}

void OsmBuilderWindow::onClickRenderSolid(wxCommandEvent& event) {
	m_app->SetRender(Render::BUILDINGS);
}

void OsmBuilderWindow::onClickRenderWireframe(wxCommandEvent& event) {
	m_app->SetRender(Render::RECOGNITION);
}

void OsmBuilderWindow::onClickRenderPoints(wxCommandEvent& event) {
	m_app->SetRender(Render::FOOTPRINTS);
}